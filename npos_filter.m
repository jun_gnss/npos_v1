function [dataXk,dataPk,ins] = npos_filter(dataIMU,dataGNSS,sLC,fb_flag)
sLC.fb_flag = fb_flag;

% Alignment
sLC.af_flag = 'align';
[sAlign,~] = npos_init(dataIMU,dataGNSS,sLC,fb_flag);

% Filter
sLC.af_flag = 'filter';
% try
[dataXk,dataPk,ins] = npos_lc(dataIMU,dataGNSS,sAlign,sLC );
% catch
%     disp('exception');
% end