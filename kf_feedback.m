function  [kf, ins] = kf_feedback(kf, ins, method_id )

if strcmp(method_id,'EXP1')
%     if kf.T_fb~=T_fb
%         kf.T_fb = T_fb;
%         %         kf.coef_fb = (1.0-exp(-kf.T_fb./kf.xtau));
%         %         kf.coef_fb = ar1coefs(kf.T_fb, kf.xtau);
%         xtau = kf.xtau;
%         xtau(kf.xtau<kf.T_fb) = kf.T_fb;  kf.coef_fb = kf.T_fb./xtau;  %2015-2-22
%     end
%     xfb_tmp = kf.coef_fb.*kf.xk;
        xfb_tmp =  kf.xk;
    ins.qnb = qdelphi(ins.qnb, xfb_tmp(1:3));
    ins.vn = ins.vn - xfb_tmp(4:6);
    ins.pos = ins.pos - xfb_tmp(7:9);
    ins.eb = ins.eb - xfb_tmp(10:12);
    ins.db = ins.db - xfb_tmp(13:15);
%     ins.Kg = ins.Kg - xfb_tmp(16:18);
%     ins.Ka = ins.Ka - xfb_tmp(19:21);
    [ins.qnb, ins.att, ins.Cnb] = attsyn(ins.qnb);
    ins.avp = [ins.att; ins.vn; ins.pos];
%     kf.xk = zeros(15,1);
    kf.xk(1:15,1) = 0;
end