function write_sbet(dataSBET,sbetRMS,sFile,SESSION)

sbet_file = strcat(sFile.SESSION_OUTPUT,SESSION,'_npos.out');
sbetrms_file = strcat(sFile.SESSION_OUTPUT,SESSION,'_npos.rms');
outFile=fopen(sbet_file,'w');
fwrite(outFile,dataSBET','double');
fclose(outFile);

outFile=fopen(sbetrms_file,'w');
fwrite(outFile,sbetRMS','double');
fclose(outFile);