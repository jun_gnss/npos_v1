function Pxk = get_Pxk(method_id,af_flag)
glvs;
if strcmp (method_id,'EXP1')
    if strcmp(af_flag,'filter')
         Pxk = diag([[0.02;0.02;0.03]*glv.deg; [.01;.01;.01]; [0.2/glv.Re;0.2/glv.Re;0.2]; [0.1;0.1;0.1]*glv.dph; [0.3;0.3;0.3]*glv.mg;[100;100;100]*glv.ppm;[300;300;300]*glv.ppm])^2;
    elseif strcmp(af_flag,'align')
        Pxk = diag([[10;10;20]*glv.deg; [2;2;2]; [3/glv.Re;3/glv.Re;3]; [1;1;1]*glv.dph; [3;3;3]*glv.mg;[100;100;100]*glv.ppm;[300;300;300]*glv.ppm])^2;
    end
end