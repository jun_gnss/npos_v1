function [Xk, Pxk] = kfupdate2(Fikk_1, Qk, Xk, Pxk, Hk, Rk, Zk)
 
if nargin == 7
    kf.Phikk_1 = Fikk_1;
    kf.Qk = Qk;
    kf.xk = Xk;
    kf.Pxk = Pxk;
    kf.Hk = Hk;
    kf.Rk = Rk;
     yk = Zk;
    
    kf.xkk_1 = kf.Phikk_1*kf.xk;
    kf.Pxkk_1 = kf.Phikk_1*kf.Pxk*kf.Phikk_1' + kf.Qk;
    kf.Pxykk_1 = kf.Pxkk_1*kf.Hk';  % see my textbook (9.3-40)
    kf.Pykk_1 = kf.Hk*kf.Pxykk_1 + kf.Rk;
    kf.Kk = kf.Pxykk_1*invbc(kf.Pykk_1); % kf.Kk = kf.Pxykk_1*kf.Pykk_1^-1;
    kf.ykk_1 = kf.Hk*kf.xkk_1;
    kf.xk = kf.xkk_1 + kf.Kk*(yk-kf.ykk_1);
    kf.Pxk = kf.Pxkk_1 - kf.Kk*kf.Pykk_1*kf.Kk';
    kf.Pxk = (kf.Pxk+kf.Pxk')*0.5; % symmetrization & forgetting factor 's'
    
    Xk = kf.xk;
    Pxk = kf.Pxk;
elseif nargin == 4
    kf.Phikk_1 = Fikk_1;
    kf.Qk = Qk;
    kf.xk = Xk;
    kf.Pxk = Pxk;
    
    kf.xk = kf.Phikk_1*kf.xk;
    kf.Pxk = kf.Phikk_1*kf.Pxk*kf.Phikk_1' + kf.Qk;
    
    Xk = kf.xk;
    Pxk = kf.Pxk;
end
