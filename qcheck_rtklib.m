function Qmsg = qcheck_rtklib(Struct_rtklib)


% Variables Initialization
FILE_EVALUATION_TEMPLATE = 'evaluation_cfg.txt';
Struct_mission_cfg = Struct_cfg_initial(SESSION); 
Struct_DIR         = Struct_mission_cfg.Struct_DIR;
Struct_FILE        = Struct_mission_cfg.Struct_FILE;
Struct_BASE        = Struct_mission_cfg.Struct_BASE;
Struct_GENERAL_CFG = Struct_mission_cfg.Struct_GENERAL_CFG;
Struct_SENSOR      = Struct_mission_cfg.Struct_SENSOR;

IMU_INTERVAL            = 1/Struct_GENERAL_CFG.IMU_HERTZ;
LEAP_SECONDS            = Struct_GENERAL_CFG.LEAP_SECONDS;
GPS_INTERVAL            = 1;%Struct_GENERAL_CFG.GNSS_INTERVAL

FILE_RTKLIB_QCHECK = strcat(Struct_DIR.LOG,'rtklib_qcheck.log');
fidout = fopen(FILE_RTKLIB_QCHECK,'w+');
n_base = length(Struct_BASE);


% fprintf(fidout,'    MAX   of double difference excluding the first and last %f minutes\n',Struct_evaluation_cfg.MINUTES_IGNORE_GNSS);

% % (1) TEQC Report Summary for Independent Base
% fprintf(fidout,'***************************************************\n');
% fprintf(fidout,'              RTKLIB GNSS BASE QCHECK \n');
% fprintf(fidout,'                 TEQC SUMMARY \n');
% fprintf(fidout,'***************************************************\n');
% for i = 1:n_base
%     try
%         system(char(strcat('teqc +qc -plot -O.int 1',{' '},Struct_BASE(i).OFILE)));
%         temp_sum_file = Struct_BASE(i).OFILE;
%         temp_sum_file(end) = 'S';
%         Struct_BASE(i).Struct_Qflag_teqc = read_teqc_s (temp_sum_file);% To do
%     catch
%         fprintf(1,'TEQC of %s failed !\n',Struct_BASE(i).OFILE);
%     end
% end

% (2) Forward-Backward Evaluation for Independent Base
fprintf(fidout,'***************************************************\n');
fprintf(fidout,'              RTKLIB GNSS BASE QCHECK \n');
fprintf(fidout,'                 [Forward-Backward]\n');
fprintf(fidout,'***************************************************\n');
DIR_rtklib_F  = Struct_DIR.RTKLIB_F;
DIR_rtklib_B  = Struct_DIR.RTKLIB_B;
DIR_rtklib_S  = Struct_DIR.RTKLIB_S;
DIR_NPOS_LOG  = Struct_DIR.LOG;
epochs_IGNORE = Struct_evaluation_cfg.MINUTES_IGNORE_GNSS*60*GPS_INTERVAL;
RTKLIB_ID     = Struct_GENERAL_CFG.RTKLIB_ID;
GNSS_q_flag_std  = zeros(1,3);
GNSS_q_flag_mean = zeros(1,3);
GNSS_q_flag_max  = zeros(1,3);

for i = 1:n_base
    try
        posfile_1 = strcat(DIR_rtklib_F,SESSION,'_',RTKLIB_ID,'_',Struct_BASE(i).NAME,'.pos');
        posfile_2 = strcat(DIR_rtklib_B,SESSION,'_',RTKLIB_ID,'_',Struct_BASE(i).NAME,'.pos');
        data_RTKLIB_1 = rtklib2data (posfile_1);
        data_RTKLIB_2 = rtklib2data (posfile_2);
        cmp_rtklib_internal = [];
        cmp_rtklib_internal = cmp_two_gnss(data_RTKLIB_1,data_RTKLIB_2,epochs_IGNORE);
        fprintf(fidout,'# Base: %s\n',Struct_BASE(i).NAME);
        cmp_gnss_printf(fidout, cmp_rtklib_internal);
%         Struct_BASE(i).Struct_Qflag_fwd_bkw = ;
        
        
        % Draw Figures
        if Struct_evaluation_cfg.FLAG_DRAW_GNSS_DIFF_FIGURE == 1
            h=figure;
            subplot(3,1,1);plot(cmp_rtklib_internal.diff_plh(:,1),cmp_rtklib_internal.diff_plh(:,2)');
            set(gca, 'YLim',[-0.25 0.25]);
            title('latitude ');ylabel('meter');
            subplot(3,1,2);plot(cmp_rtklib_internal.diff_plh(:,1),cmp_rtklib_internal.diff_plh(:,3));
            set(gca, 'YLim',[-0.25 0.25]);
            subplot(3,1,3);plot(cmp_rtklib_internal.diff_plh(:,1),cmp_rtklib_internal.diff_plh(:,4));
            set(gca, 'YLim',[-0.25 0.25]);
            title('height ');ylabel('meter');
            xlabel({'GPS time',strcat('QC forward-backward of [',Struct_BASE(i).NAME,'] ')});
            fig_name = strcat(DIR_NPOS_LOG,'QC of [',Struct_BASE(i).NAME,']_','forward_backward_plot.jpg');
            saveas(h,fig_name,'jpg');
        end
        
        
        
%         rtklib_file_cmp (file1, file2,
%         system(char(strcat('teqc +qc -plot -O.int 1',{' '},Struct_BASE(i).OFILE)));
%         temp_sum_file = Struct_BASE(i).OFILE;
%         temp_sum_file(end) = 'S';
%         Struct_BASE(i).Struct_teqc = read_teqc_s (temp_sum_file);% To do
    catch
        fprintf(1,'TEQC of %s failed !\n',Struct_BASE(i).OFILE);
    end
end

% Base-Base Evaluation

fprintf(fidout,'[Base-Base]\n');
if n_base == 1
    fprintf(fidout,'There is only one base station, no internal site quality check available !\n');
else
    comb_array = combntns(1:n_base,2);
    for i = 1:size(comb_array,1)
        index_1 = comb_array(i,1);
        index_2 = comb_array(i,2);
        posfile_1 = strcat(DIR_rtklib_S,SESSION,'_',RTKLIB_ID,'_',Struct_BASE(index_1).NAME,'.pos');
        posfile_2 = strcat(DIR_rtklib_S,SESSION,'_',RTKLIB_ID,'_',Struct_BASE(index_2).NAME,'.pos');
        data_RTKLIB_1 = rtklib2data (posfile_1);
        data_RTKLIB_2 = rtklib2data (posfile_2);
        cmp_rtklib_internal = [];
        index_temp = find (data_RTKLIB_1(:,1) ==0,1);
        if ~isempty(index_temp)
            data_RTKLIB_1(index_temp:end,1) = data_RTKLIB_1(index_temp:end,1)+604800;
        end
        index_temp = find (data_RTKLIB_2(:,1) ==0,1);
        if ~isempty(index_temp)
            data_RTKLIB_2(index_temp:end,1) = data_RTKLIB_2(index_temp:end,1)+604800;% Forward too
        end
        cmp_rtklib_internal = cmp_two_gnss(data_RTKLIB_1,data_RTKLIB_2,epochs_IGNORE);
        
        fprintf(fidout,'# %s between %s : \n',Struct_BASE(index_1).NAME,Struct_BASE(index_2).NAME);
        fprintf(fidout,'    STD   of differnce in lat/lon/height (unit:meter)      --: %+6.4f  %+6.4f  %+6.4f\n',cmp_rtklib_internal.diff_plh_std);
        fprintf(fidout,'    MEAN  of differnce in lat/lon/height (unit:meter)      --: %+6.4f  %+6.4f  %+6.4f\n',cmp_rtklib_internal.diff_plh_mean);
        fprintf(fidout,'    MAX   of differnce in lat/lon/height (unit:meter)      --: %+6.4f  %+6.4f  %+6.4f\n',cmp_rtklib_internal.diff_plh_max);
        fprintf(fidout,'    RATIO of large difference in position                  --: %+6.4f  %+6.4f  %+6.4f\n',cmp_rtklib_internal.bad_ratio);
        fprintf(fidout,'    MAX   of double difference excluding the first and last %f minutes\n',Struct_evaluation_cfg.MINUTES_IGNORE_GNSS);
        fprintf(fidout,'                          lat/lon/height (unit:meter)      --: %+6.4f  %+6.4f  %+6.4f\n',cmp_rtklib_internal.diff_plh_max_2);
        if cmp_rtklib_internal.diff_plh_max_2(1) > Struct_evaluation_cfg.LIMIT_MAX_DIFF_RTKLIB_S_AND_S_1
            flag_rtklib_quality = 1;
            fprintf(1,strcat('Latitude solution between [',Struct_BASE(index_1).NAME,'] and [',Struct_BASE(index_2).NAME,'] is not good !\n'));
        end
        if cmp_rtklib_internal.diff_plh_max_2(2) > Struct_evaluation_cfg.LIMIT_MAX_DIFF_RTKLIB_S_AND_S_1
            flag_rtklib_quality = 1;
            fprintf(1,strcat('Longitude solution between [',Struct_BASE(index_1).NAME,'] and [',Struct_BASE(index_2).NAME,'] is not good !\n'));
        end
        if cmp_rtklib_internal.diff_plh_max_2(3) > Struct_evaluation_cfg.LIMIT_MAX_DIFF_RTKLIB_S_AND_S_2
            flag_rtklib_quality = 1;
            fprintf(1,strcat('Height solution between [',Struct_BASE(index_1).NAME,'] and [',Struct_BASE(index_2).NAME,'] is not good !\n'));
        end
        % Draw Figures
        if Struct_evaluation_cfg.FLAG_DRAW_GNSS_DIFF_FIGURE == 1
            h=figure;
            subplot(3,1,1);plot(cmp_rtklib_internal.diff_plh(:,1),cmp_rtklib_internal.diff_plh(:,2)');
            set(gca, 'YLim',[-0.25 0.25]);
            title('latitude ');ylabel('meter');
            subplot(3,1,2);plot(cmp_rtklib_internal.diff_plh(:,1),cmp_rtklib_internal.diff_plh(:,3));
            set(gca, 'YLim',[-0.25 0.25]);
            subplot(3,1,3);plot(cmp_rtklib_internal.diff_plh(:,1),cmp_rtklib_internal.diff_plh(:,4));
            set(gca, 'YLim',[-0.25 0.25]);
            title('height ');ylabel('meter');
            xlabel({'GPS time',strcat('QC between [',Struct_BASE(index_1).NAME,'] and [',Struct_BASE(index_2).NAME,']')});
            fig_name = strcat(DIR_NPOS_LOG,'QC_between_',Struct_BASE(index_1).NAME,'_',Struct_BASE(index_2).NAME,'_plot.jpg');
            saveas(h,fig_name,'jpg');
        end
    end
end

% Baseline-PPP Evaluation

% Generate New Variable of Bases and Save Evaluation Result


disp('RTKLIB position internal check is DONE !');

close all;

%*************************************************************************
%% Delete unnecessary variables and files
%*************************************************************************

fclose(fidout);