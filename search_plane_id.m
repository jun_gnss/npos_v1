function plane_id = search_plane_id(sFile)
temp_dir_name = sFile.SESSION;
if strcmp(temp_dir_name(1:3),'023')
    plane_id = '023';
    return;
elseif strcmp(temp_dir_name(1:3),'020')
    plane_id = '020';
    return;
end
dir_session = sFile.SESSION_INPUT;
result_dir = dir(dir_session);
n = length(result_dir);
for i = 1:n
    if result_dir(i).isdir
        temp_dir_name = result_dir(i).name;
        if length(temp_dir_name)>3
            if strcmp(temp_dir_name(1:3),'023')
                plane_id = '023';
                return;
            elseif strcmp(temp_dir_name(1:3),'020')
                plane_id = '020';
                return;
            end
        end
    end
end
error('Cannot automatically get the plane id !\n');
