function [wnie, wnen, gn, retp] = earth_old(pos, vn)
%
%   INPUT:
%       pos: latitude/longitude/elevation, rad/rad/meter
%       vn:
%
%   wnie:the turn rate of the Earth expressed in the local geographic frame; <SINS Technology> page 47, (3.72) book used NED navigation frame
%   wnen:the turn rate of the local geographic frame with respect to the Earth-fixed frame; the transport rate, page 50 (3.87)<SINS Technology>
%   RMh: Meridional Earth radius http://en.wikipedia.org/wiki/Earth_radius#Volumetric_radius
%         or <SINS Technology> Page 49 (3.83) and (3.84)
%   RNh: Normal Earth radius/a transverse radius of curvature
%   gn: Normal gravity at specific location


glv_f = 1/298.257;
glv_wie = 7.2921151467e-5;
glv_Re = 6378137;
glv_g0 = 9.7803267714;
glv_e = sqrt(2*glv_f-glv_f^2);
glv_e2 = glv_e^2;


if nargin==1
    vn = [0; 0; 0];
end
sl=sin(pos(1)); cl=cos(pos(1)); tl=sl/cl; sl2=sl*sl; sl4=sl2*sl2;
wnie = glv_wie*[0; cl; sl];
sq = 1-glv_e2*sl2; sq2 = sqrt(sq);
RMh = glv_Re*(1-glv_e2)/sq/sq2+pos(3);
RNh = glv_Re/sq2+pos(3);
wnen = [-vn(2)/RMh; vn(1)/RNh; vn(1)/RNh*tl];
g = glv_g0*(1+5.27094e-3*sl2+2.32718e-5*sl4)-3.086e-6*pos(3); %  grs80 ellipsoid, normal gravity, page 56 <SINS>(3.89)
gn = [0;0;-g];
retp.g = g;
retp.sl = sl; retp.cl = cl; retp.tl = tl; retp.sl2 = sl2;
retp.rmh = RMh; retp.rnh = RNh;
