function kf = kfupdate(kf, yk, TimeMeasBoth)
% Discrete-time Kalman filter.
%
% Prototype: kf = kfupdate(kf, yk, TimeMeasBoth)
% Inputs: kf - Kalman filter structure array
%         yk - measurement vector
%         TimeMeasBoth - described as follows,
%            TimeMeasBoth='T' (or nargin==1) for time updating only,
%            TimeMeasBoth='M' for measurement updating only,
%            TimeMeasBoth='B' (or nargin==2) for both time and
%                             measurement updating.
% Output: kf - Kalman filter structure array after time/meas updating
% Note: the Kalman filter stochastic models is
%    xk = Phikk_1*xk_1 + wk_1
%    yk = Hk*xk + vk
%    where E[wk]=0, E[vk]=0, E[wk*wk']=Qk, E[vk*vk']=Rk, E[wk*vk']=0
%
% See also  kfinit, kfupdatesq, kffk, kfhk, kfc2d, kffeedback, kfplot, RLS, ekf, ukf.

% Copyright(c) 2009-2014, by Gongmin Yan, All rights reserved.
% Northwestern Polytechnical University, Xi An, P.R.China
% 08/12/2012, 29/08/2013
if nargin==1;
    TimeMeasBoth = 'T';
elseif nargin==2
    TimeMeasBoth = 'B';
end

if TimeMeasBoth=='T'            % Time Updating
    kf.xk = kf.Phikk_1*kf.xk;
    kf.Pxk = kf.Phikk_1*kf.Pxk*kf.Phikk_1' + kf.Qk;
else
    if TimeMeasBoth=='M'        % Meas Updating
        kf.xkk_1 = kf.xk;
        kf.Pxkk_1 = kf.Pxk;
    elseif TimeMeasBoth=='B'    % Time & Meas Updating
        kf.xkk_1 = kf.Phikk_1*kf.xk;
        kf.Pxkk_1 = kf.Phikk_1*kf.Pxk*kf.Phikk_1' + kf.Qk;
    else
        error('TimeMeasBoth input error!');
    end
    kf.Pxykk_1 = kf.Pxkk_1*kf.Hk';  % see my textbook (9.3-40)
    kf.Pykk_1 = kf.Hk*kf.Pxykk_1 + kf.Rk;
    kf.Kk = kf.Pxykk_1*invbc(kf.Pykk_1); % kf.Kk = kf.Pxykk_1*kf.Pykk_1^-1;
    kf.ykk_1 = kf.Hk*kf.xkk_1;
    kf.xk = kf.xkk_1 + kf.Kk*(yk-kf.ykk_1);
    kf.Pxk = kf.Pxkk_1 - kf.Kk*kf.Pykk_1*kf.Kk';
    kf.Pxk = (kf.Pxk+kf.Pxk')*(kf.s/2); % symmetrization & forgetting factor 's'
end
