function [sIns_fine,sAlineFine,sQmsg] = align_fine(dataIMU,dataGNSS,sIns,sLC,fb_flag)
% sLC.fb_flag = fb_flag;
sLC.af_flag = 'align';

t_initial = sIns.time;
align_mins = sLC.ALIGNMENT_MINS;
align_iteration_num = sLC.ITERATION_MAXNUM;
imu_hz = sLC.IMU_HERTZ; 

[t_align,epoch_align_imu_start] = find_close_epoch(dataIMU(:,1),t_initial);
reverse = reverse_flag(fb_flag);
epoch_align_imu_end = epoch_align_imu_start+reverse*align_mins*60*imu_hz;
if epoch_align_imu_end>length(dataIMU)||epoch_align_imu_end<1
    fprintf(1,'There is not enough epochs for alignment function, please reset the ALIGNMENT_MINS in the configuration file !\n');
    fprintf(1,'The %s alignment failed and program quit!\n',fb_flag);
    return;
else
    if reverse == 1
        dataIMU_align = dataIMU(epoch_align_imu_start:epoch_align_imu_end,:);
    elseif reverse == -1
        dataIMU_align = dataIMU(epoch_align_imu_end:epoch_align_imu_start,:);
    end
    clear dataIMU;
end

switch upper(fb_flag)
    case 'FORWARD'
        process_id_array = {'forward','backward'};
%         index_epoch_array = [length(dataIMU_align),1];
    case 'BACKWARD'
        process_id_array = {'backward','forward'};
%         index_epoch_array = [1,length(dataIMU_align)];
end

iteration_num = 0;
while 1
    iteration_num = iteration_num+1;
    fprintf(1,'The fine alignment starts processing with iteration number [%d]...\n',iteration_num);
    %     [data_RSGPS_align,var_Xk_align] = rsgps_main(Struct_align,dataIMU_align,data_gnss,Struct_mission_cfg,process_id_array{1},'align');
    sLC.fb_flag = process_id_array{1};
    [~,~,sIns] = npos_lc(dataIMU_align,dataGNSS,sIns,sLC );
    
    sLC.fb_flag = process_id_array{2};
    [~,~,sIns] = npos_lc(dataIMU_align,dataGNSS,sIns,sLC );
    
    % Record historic result
    if iteration_num == 1
        sInsOld = sIns;
        continue;
    else 
        % Decide whether quit the loop of fine alignment
        [fine_flag,sAlineFine] = is_align_good(sInsOld,sIns);
        if fine_flag == 1
            sIns_fine = sIns;
            sIns_fine.time = t_align;
            return;
        end
        sInsOld = sIns;
    end
    
    % Accept the second option for fine alignment result
    if iteration_num == align_iteration_num
        sIns_fine = sIns;
        sIns_fine.time = t_align;
        sQmsg = 'The fine alignment result is calculated with maximum iteration number';
    end
end
 