function sLC = npos_initLC(sNpos,sImu,sLaMa)
% Program:
%   Initialize the GNSS/IMU Loose Coupled algorithm parameters.
 
 
sLC.fb_flag = [];
sLC.af_flag = []; 
sLC.METHOD_ID = sNpos.METHOD_ID;
sLC.KF_ID = sNpos.KF_ID;
sLC.OBS_ID = sNpos.OBS_ID ;
sLC.TIMESYS_ID = sNpos.TIMESYS_ID;  
sLC.COARSE_VE_LIMIT = sNpos.COARSE_VE_LIMIT;
sLC.COARSE_VN_LIMIT = sNpos.COARSE_VN_LIMIT;
sLC.ITERATION_MAXNUM = sNpos.ITERATION_MAXNUM ;
sLC.ALIGN_APPROACH = sNpos.ALIGN_APPROACH ;
sLC.ALIGNMENT_MINS = sNpos.ALIGNMENT_MINS; 
sLC.COARSE_VE_LIMIT = sNpos.COARSE_VE_LIMIT;
sLC.COARSE_VN_LIMIT = sNpos.COARSE_VN_LIMIT;
sLC.ITERATION_MAXNUM = sNpos.ITERATION_MAXNUM;
sLC.SECONDS_DELETE_GNSS = sNpos.SECONDS_DELETE_GNSS;
sLC.SECONDS_DELETE_IMU = sNpos.SECONDS_DELETE_IMU;

sLC.IMU_HERTZ = sImu.IMU_HERTZ;  
sLC.sImu = sImu;
sLC.sLaMa = sLaMa;


% af_flag = sLC.type; %Loose couple for aligning or filtering
% fb_flag = sLC.direction;
% kf_flag = sLC.modle; % Default 21 with OBS
% ts0 = sLC.sample;
% xk_num = sLC.xk_number;
% kf_obs = sLC.obs_type;
% sImu = sLC.sImu; % IMU hardware parameters