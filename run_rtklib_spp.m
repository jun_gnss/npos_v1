function file_spp = run_rtklib_spp (FILE_ROVER_O,FILE_ROVER_N,FILE_POS_SPP)
 
 CMD_TEMP_RTKLIB_SPP = strcat('rnx2rtkp -p 0 -m 15 -o',{' '},FILE_POS_SPP,{' '},FILE_ROVER_O,{' '}, ...
     FILE_ROVER_N);
 
cmd_rtklib_spp = strrep(CMD_TEMP_RTKLIB_SPP,'SESSION',SESSION);
system(char(cmd_rtklib_spp));
file_spp =  strrep(FILE_POS_SPP,'SESSION',SESSION);