function  ins = INSReverse( ins)
 
ins.eth.wie = -ins.eth.wie;
ins.vn = -ins.vn; 
ins.eb = -ins.eb; 
ins.tDelay = -ins.tDelay;
