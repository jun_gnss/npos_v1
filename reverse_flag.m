function reverse = reverse_flag(fb_flag)

if strcmp(fb_flag,'forward')
    reverse = 1;
elseif strcmp(fb_flag,'backward')
    reverse = -1;
else
    error('The forward/backward flag is incorrect!');
end