function sFile = read_path_cfg(file_cfg,session)

fid = fopen(file_cfg);
sFile = struct('SESSION_INPUT',[],'SESSION_OUTPUT',[],'RTKLIB_ID',[],...
    'PRODUCTS_DCB',[],'PRODUCTS_CODE',[],'PRODUCTS_IGS',[],'PRODUCTS_ATX',[],...
    'SESSION_EXTRACT',[], 'SESSION_GNSS',[],'SESSION_PROC',[],...
    'FILE_POSGNSS',[], 'FILE_IMU',[],'FILE_SBET',[],...
    'FILE_ROVER_G',[], 'FILE_ROVER_N',[],'FILE_ROVER_O',[]);
sFile.SESSION = session;
while 1
    if (feof(fid)==1)
        break
    end
    lin = fgets(fid);
    if strfind(lin,'%')
        continue;
    end
    lin_cell =  textscan(lin,'%s %s');
    lin_flag = lin_cell{1}(1);
    switch char(lin_flag)
        
        case 'SESSION_INPUT'
            sFile.SESSION_INPUT = char(lin_cell{2});
        case 'SESSION_OUTPUT'
            sFile.SESSION_OUTPUT = char(lin_cell{2});
        case 'PRODUCTS_CODE'
            sFile.PRODUCTS_CODE = char(lin_cell{2});
        case 'PRODUCTS_IGS'
            sFile.PRODUCTS_IGS = char(lin_cell{2});
        case 'PRODUCTS_ATX'
            sFile.PRODUCTS_ATX= char(lin_cell{2});
        case 'PRODUCTS_DCB'
            sFile.PRODUCTS_DCB= char(lin_cell{2});
        case 'SESSION_EXTRACT'
            sFile.SESSION_EXTRACT = char(lin_cell{2});
        case 'SESSION_GNSS'
            sFile.SESSION_GNSS = char(lin_cell{2});
        case 'SESSION_PROC'
            sFile.SESSION_PROC = char(lin_cell{2});
        case 'FILE_POSGNSS'
            sFile.FILE_POSGNSS = char(lin_cell{2});
        case 'FILE_IMU'
            sFile.FILE_IMU = char(lin_cell{2});
        case 'FILE_SBET'
            sFile.FILE_SBET = char(lin_cell{2});
        case 'FILE_ROVER_O'
            sFile.FILE_ROVER_O = char(lin_cell{2});
        case 'FILE_ROVER_N'
            sFile.FILE_ROVER_N = char(lin_cell{2});
        case 'FILE_ROVER_G'
            sFile.FILE_ROVER_G = char(lin_cell{2});
    end
end
fclose(fid); 
sFile.SESSION_INPUT = strrep(sFile.SESSION_INPUT,'SESSION',session);
sFile.SESSION_OUTPUT = strrep(sFile.SESSION_OUTPUT,'SESSION',session);
sFile.SESSION_EXTRACT = strrep(sFile.SESSION_EXTRACT,'SESSION',session);
sFile.SESSION_GNSS = strrep(sFile.SESSION_GNSS,'SESSION',session);
sFile.SESSION_PROC = strrep(sFile.SESSION_PROC,'SESSION',session);

sFile.FILE_POSGNSS = strrep(sFile.FILE_POSGNSS,'SESSION',session);
sFile.FILE_IMU = strrep(sFile.FILE_IMU,'SESSION',session);
sFile.FILE_SBET = strrep(sFile.FILE_SBET,'SESSION',session);
sFile.FILE_ROVER_O = strrep(sFile.FILE_ROVER_O,'SESSION',session);
sFile.FILE_ROVER_N = strrep(sFile.FILE_ROVER_N,'SESSION',session);
sFile.FILE_ROVER_G = strrep(sFile.FILE_ROVER_G,'SESSION',session);

sFile.FILE_POSGNSS = strcat(sFile.SESSION_GNSS,sFile.FILE_POSGNSS);
sFile.FILE_IMU = strcat(sFile.SESSION_EXTRACT,sFile.FILE_IMU);
sFile.FILE_SBET = strcat(sFile.SESSION_PROC,sFile.FILE_SBET);
%