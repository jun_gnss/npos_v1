function out = diag_posdef(in)
 
% The function computes a positive semi-definite matrix 'out' that is 
% closest to a symmetric covariance matrix 'in' which is not positive
% semi-definite. 
 

% %Set the matrix as symmetric
% %Approach (1)
% in= tril(in)+tril(in)'-diag(diag(in));
% %Set the diagonal negative values as zero
% temp_idx=diag(in)<0;
% in(temp_idx,temp_idx)=0.001;
out=0.5*(in+in');

 

% % Approach (2) Paul's advice
% %  [u,s,v] = eig(in);
% %  s(diag(s)<eps)=0;
% %  out=u*s*v';
% 
%  [v , d] = eig(in);
%  s=(d);
%  s(diag(s)<eps)=0;
%  d=(s);
%  out=v*d*v';
%  
%  if sum(isinf(out))>0
%      error('There are infinity elements');
%  end