function [sIns,sQmsg] = npos_init(dataIMU,dataGNSS,sLC,fb_flag)
% Program:
%	Alignment, to get the initial IMU attitude, velocity, position
% information.
%
% Determine the 


% Coarse Alignment
[sIns_coarse] = align_coarse(dataIMU,dataGNSS,sLC,fb_flag);

% Fine Alignment
sLC.af_flag = 'align';
[sIns,sQmsg] = align_fine(dataIMU,dataGNSS,sIns_coarse,sLC,fb_flag);

% Recording message of quality issues