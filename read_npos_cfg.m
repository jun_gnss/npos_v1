function sNpos = read_npos_cfg(file_cfg,sFile)
%   Program:
%       Read npos configuration parameters
%   INPUT:
%       file_cfg : Npos_Config.txt
%       sFile: The directory and file information for working session
%   OUTPUT:
%       sNpos : Struct of configuration

fid = fopen(file_cfg);
sNpos = struct('METHOD_ID',[],'PRODUCT_ID',[],'RTKLIB_ID',[],...
    'KF_ID',[],'OBS_ID',[],'TIMESYS_ID',[],'PLANE_ID',[], ...
    'MISSION_HOURS',[],'LEAP_SECONDS',[],'ALIGNMENT_MINS',[],...
    'COARSE_VE_LIMIT',[],'COARSE_VN_LIMIT',[],...
    'ALIGN_APPROACH',[], 'ITERATION_MAXNUM',[],...
    'SECONDS_DELETE_GNSS',[],'SECONDS_DELETE_IMU',[]);
while 1
    if (feof(fid)==1)
        break
    end
    lin = fgets(fid);
    if strfind(lin,'%')
        continue;
    end
    lin_cell =  textscan(lin,'%s %s');
    lin_flag = lin_cell{1}(1);
    switch char(lin_flag)
        
        case 'METHOD_ID'
            sNpos.METHOD_ID = char(lin_cell{2});
        case 'PRODUCT_ID'
            sNpos.PRODUCT_ID = char(lin_cell{2});
        case 'TIMESYS_ID'
            sNpos.TIMESYS_ID = char(lin_cell{2});
        case 'MACHINE_ID'
            sNpos.MACHINE_ID = char(lin_cell{2});
        case 'KF_ID'
            sNpos.KF_ID= char(lin_cell{2});
        case 'OBS_ID'
            sNpos.OBS_ID= char(lin_cell{2});
        case 'RTKLIB_ID'
            sNpos.RTKLIB_ID = char(lin_cell{2});
        case 'MISSION_HOURS'
            sNpos.MISSION_HOURS = str2double(lin_cell{2});
        case 'ALIGNMENT_MINS'
            sNpos.ALIGNMENT_MINS = str2double(lin_cell{2});
        case 'LEAP_SECONDS'
            sNpos.LEAP_SECONDS = str2double(lin_cell{2});
        case 'ALIGN_APPROACH'
            sNpos.ALIGN_APPROACH = char(lin_cell{2});
        case 'COARSE_VE_LIMIT'
            sNpos.COARSE_VE_LIMIT = str2double(lin_cell{2});
        case 'COARSE_VN_LIMIT'
            sNpos.COARSE_VN_LIMIT = str2double(lin_cell{2});
        case 'ITERATION_MAXNUM'
            sNpos.ITERATION_MAXNUM = str2double(lin_cell{2});
        case 'SECONDS_DELETE_GNSS'
            sNpos.SECONDS_DELETE_GNSS = str2double(lin_cell{2});
        case 'SECONDS_DELETE_IMU'
            sNpos.SECONDS_DELETE_IMU = str2double(lin_cell{2});
    end
end
fclose(fid);
% Automatically find the plane id for extract the correct lever arm
% information
plane_id = search_plane_id(sFile);
sNpos.PLANE_ID = plane_id;
