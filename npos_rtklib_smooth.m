function npos_rtklib_smooth (MISSION,rtklib_s_flag,Array_Base_selection)
if nargin < 2
    rtklib_s_flag = 0;
    Array_Base_selection = 0;
end
t_cost_beg = clock;
function_name = mfilename('fullpath');

% MISSION = '13102301';

DIRECTION = 'COMBINED';

%*************************************************************************
%%  (1) Read NPOS Mission Configuration File
%*************************************************************************
if ispc
    file_NPOS_cfg_template = 'configuration_template_local.txt';
    file_NPOS_dir_template = 'directory_template_local.txt';
else
    file_NPOS_cfg_template = 'configuration_template.txt';
    file_NPOS_dir_template = 'directory_template.txt';
end

Struct_temp_config = read_cfg_temp (file_NPOS_cfg_template);
Struct_dir_file    = read_dir_temp(file_NPOS_dir_template,Struct_temp_config,MISSION);
dir_Configuration  = Struct_dir_file.DIR_NPOS_CONFIG;

%*************************************************************************
%                         Read mission configuration file
%*************************************************************************
file_Mission_configuration = strcat(dir_Configuration,MISSION,'_cfg.conf');
if ~exist(file_Mission_configuration,'file')
    create_mission_cfg_file (MISSION,file_NPOS_cfg_template,file_NPOS_dir_template);
end
Struct_mission_cfg = read_mission_cfg (file_Mission_configuration);

Struct_DIR         = Struct_mission_cfg.Struct_DIR;
Struct_FILE        = Struct_mission_cfg.Struct_FILE;
Struct_BASE        = Struct_mission_cfg.Struct_BASE;
Struct_GENERAL_CFG = Struct_mission_cfg.Struct_GENERAL_CFG;
Struct_SENSOR      = Struct_mission_cfg.Struct_SENSOR;

switch DIRECTION
    case 'FORWARD'
        DIR_result = Struct_DIR.RTKLIB_F;
        DIR_log    = Struct_DIR.LOG;
        file_NPOS_log = strcat(DIR_log,MISSION,'_rtklib_F.log');
        file_stat      = strcat(DIR_result,MISSION,'_rtklib_F','_stat.txt');
    case 'BACKWARD'
        DIR_result = Struct_DIR.RTKLIB_B;
        DIR_log    = Struct_DIR.LOG;
        file_NPOS_log = strcat(DIR_log,MISSION,'_rtklib_B.log');
        file_stat      = strcat(DIR_result,MISSION,'_rtklib_B','_stat.txt');
    case 'COMBINED'
        DIR_rtklib_F = Struct_DIR.RTKLIB_F;
        DIR_rtklib_B = Struct_DIR.RTKLIB_B;
        DIR_result   = Struct_DIR.RTKLIB_S;
        DIR_log      = Struct_DIR.LOG;
        %*** Function Log File       ***
        file_NPOS_log = strcat(DIR_log,MISSION,'_rtklib_S.log');
        %*** Function Statistic File ***
        file_stat      = strcat(DIR_result,MISSION,'_rtklib_S','_stat.txt');
end
fidout_1 = fopen(file_NPOS_log,'w');
fidout_2 = fopen(file_stat,'w');
% Variables preallocation
rtklib_id  = Struct_GENERAL_CFG.RTKLIB_ID;
product_id = Struct_GENERAL_CFG.PRODUCT_ID;
machine_id = Struct_GENERAL_CFG.MACHINE_ID;
posfile_S_final  = strcat(DIR_result,MISSION,'_',rtklib_id,'.pos');
posfile_S_final_1 = strcat(DIR_result,MISSION,'_',rtklib_id,'.pos1');
posfile_S_final_2 = strcat(DIR_result,MISSION,'_',rtklib_id,'.pos2');

%*********************************



% Loading the RTKLIB position results with base name repectively in Forward/Backward folders
temp_dir = [];
temp_dir = dir(strcat(DIR_rtklib_F,'*.pos'));
if isempty(temp_dir)
    fprintf(fidout_1,'Warning: There is no FOWARD RTKLIB position result! \n');
else
    n_forward_pos = length(temp_dir);
    Struct_RTKLIB_F(n_forward_pos) = struct('BASE',[],'POSFILE',[]);
    for i = 1:n_forward_pos
        Struct_RTKLIB_F(i).POSFILE = strcat(DIR_rtklib_F,temp_dir(i).name);
        Struct_RTKLIB_F(i).NAME = temp_dir(i).name(end-7:end-4);
    end
end

temp_dir = [];
temp_dir = dir(strcat(DIR_rtklib_B,'*.pos'));
if isempty(temp_dir)
    fprintf(fidout_1,'Warning: There is no BACKWARD RTKLIB position result! \n');
else
    n_backward_pos = length(temp_dir);
    Struct_RTKLIB_B(n_backward_pos) = struct('BASE',[],'POSFILE',[]);
    for i = 1:n_backward_pos
        Struct_RTKLIB_B(i).POSFILE = strcat(DIR_rtklib_B,temp_dir(i).name);
        Struct_RTKLIB_B(i).NAME = temp_dir(i).name(end-7:end-4);
    end
end

%*************************************************************************
%% (2-a) RTKLIB Smooth Process Loop, combining Forward and Backward
%*************************************************************************

k = 0;
Struct_RTKLIB_S(length(Struct_RTKLIB_F)) = struct('NAME',[],'POSFILE',[]);
for i = 1:length(Struct_RTKLIB_F)
    base_F = Struct_RTKLIB_F(i).NAME;
    for j = 1:length(Struct_RTKLIB_B)
        base_B= Struct_RTKLIB_B(j).NAME;
        if strcmp(base_F,base_B)
            posfile_F = Struct_RTKLIB_F(i).POSFILE;
            posfile_B = Struct_RTKLIB_B(j).POSFILE;
            posfile_S = strcat(DIR_result,MISSION,'_',rtklib_id,'_',base_F,'.pos');
            if rtklib_s_flag == 0
                fprintf(1,'BASE [%s] rtklib position result is combining with forward and backward solution... \n',base_F);
                comb_rtklib(posfile_F,posfile_B,posfile_S,'forward-backward');
                fprintf(1,'BASE [%s] rtklib position result combination with forward and backward solution is done ! \n',base_F);
                fprintf(1,'The file is saved as %s ! \n',posfile_S);
            end
            k = k+1;
            Struct_RTKLIB_S(k).POSFILE = posfile_S;
            Struct_RTKLIB_S(k).NAME = base_F;
            break;
        else
            if j == length(Struct_RTKLIB_B)
                fprintf(fidout_1,'Warning: BASE [%s] does not have backward solution!\n',base_F);
            end
            continue;
        end
    end
end
Struct_RTKLIB_S(k+1:end) = [];

if rtklib_s_flag == 1
    m = 1;
    for j = 1:length(Array_Base_selection)
        for k = 1:length(Struct_RTKLIB_S)
            if strcmp(Array_Base_selection(j),Struct_RTKLIB_S(k).NAME)
                Struct_Base_selection(m).NAME = Struct_RTKLIB_S(k).NAME;
                Struct_Base_selection(m).POSFILE = Struct_RTKLIB_S(k).POSFILE;
                m = m+1;
            end
        end
    end
    
    %*************************************************************************
    %% (2-b) RTKLIB Smooth Process Loop, combining base by base
    %*************************************************************************
    n_comb_file = length(Struct_Base_selection);
    if isempty(Struct_Base_selection(1).NAME)
        fprintf(fidout_1,'Warning: RTKLIB smoothing solution can not be generated!\n');
        fprintf(fidout_1,'FUNCTION: NPOS_rtklib_smooth quit!\n');
        return;
    else
        if n_comb_file == 1
            fprintf(1,'-------------------------------------------------\n');
            fprintf(1,'The final smoothed rtklib solution is generated with BASE: [%s]!\n',Struct_Base_selection.NAME);
            fprintf(1,'-------------------------------------------------\n');
            copyfile(Struct_Base_selection(1).POSFILE, posfile_S_final_1);
            BASE_comb_name = Struct_Base_selection(1).NAME;
        else
            for i = 1:n_comb_file
                BASE_comb_name = Struct_Base_selection(i).NAME;
                posfile_previous_comb = strcat(DIR_result,MISSION,'_',rtklib_id,'_comb_',num2str(i),'.pos');
                fprintf(fidout_1,'-------------------------------------------------\n');
                fprintf(fidout_1,'Smooth rtklib position result is combining base station: %s  \n',BASE_comb_name);
                fprintf(1,'-------------------------------------------------\n');
                fprintf(1,'Smooth rtklib position result is combining base station: %s  \n',BASE_comb_name);
                if i == 1
                    copyfile(Struct_Base_selection(i).POSFILE, posfile_previous_comb);
                else
                    posfile_temp_1 = Struct_Base_selection(i).POSFILE;
                    posfile_temp_2 = strcat(DIR_result,MISSION,'_',rtklib_id,'_comb_',num2str(i-1),'.pos');
                    comb_rtklib(posfile_temp_1,posfile_temp_2,posfile_previous_comb,'site-site');
                    if i == n_comb_file
                        copyfile(posfile_previous_comb, posfile_S_final_1);
                        fprintf(fidout_1,'Smooth rtklib position result is DONE! \n');
                    end
                end
            end
        end
        fprintf(1,'The final smoothed rtklib file is generated and saved as %s ! \n',posfile_S_final_1);
        fprintf(fidout_1,'The final smoothed rtklib file is generated and saved as %s ! \n',posfile_S_final_1);
    end
    
%     %*************************************************************************
%     %% (2-c) RTKLIB Smooth Process Loop, combining Forward and Backward respectively
%     %*************************************************************************
%     % Generation Forward
%     aim_file_1 = strcat(Struct_DIR.RTKLIB_S,MISSION,'_',rtklib_id,'_F.pos');
%     gen_comb_rtklib(DIR_rtklib_F,aim_file_1);
%     % Generation Backward
%     aim_file_2 = strcat(Struct_DIR.RTKLIB_S,MISSION,'_',rtklib_id,'_B.pos');
%     gen_comb_rtklib(DIR_rtklib_B,aim_file_2);
%     % Generation Final Solution
%     comb_rtklib(aim_file_1,aim_file_2,posfile_S_final_2,'forward-backward');
    
    %%*******************************************************
    copyfile(posfile_S_final_1, posfile_S_final);
    %%*******************************************************
end

fclose(fidout_1);
fclose(fidout_2);
 
fprintf(1,'FUNCTION: %s has completed !\n',function_name); 