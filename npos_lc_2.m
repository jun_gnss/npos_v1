function [dataXk,dataPk,ins] = npos_lc_2(dataIMU,dataGNSS,ins,sLC )

% Program: npos_lc - NPOS Loose Coupled, using the IMU data and GNSS
%   position result to get SBET data and RMS
%
% Input:
%   dataIMU - IMU data n*7 [t,wx,wy,wz,ax,ay,az]
%   dataGNSS - structure GNSS result
%   sAlign - struct of IMU initial information as well as lever arm
%   sLC - struct of Loose Coulped,
%
% Output:

% (1) Time synchronization
% af_flag = sLC.type; %Loose couple for aligning or filtering
fb_flag = sLC.fb_flag;
method_id = sLC.METHOD_ID;
ts0 = 1/sLC.IMU_HERTZ;
kf_obs = sLC.OBS_ID;
lever = sLC.sLaMa.LEVER_ARM' ;
t_start = ins.time;
glvs;
% Initialization based on af_flag and fb_flag

[~,idx_start] = find_close_epoch(dataIMU(:,1),t_start);
reverse = reverse_flag(fb_flag);
kf = kf_initial(ins,sLC);
%
if strcmp(fb_flag,'forward')
    idx_end = size(dataIMU,1)-1;
elseif strcmp(fb_flag,'backward')
    idx_end = 2;
end
len = abs(idx_start-idx_end)+1;

if strcmp(method_id,'EXP1')
    dataXk = zeros(len,22);
    dataPk = zeros(len,22);
    obs_err = zeros(len,3);
end

% (2) Filtering
if strcmp(fb_flag,'forward')
    % Direction: forward
    for i = idx_start:reverse:idx_end
        % Get IMU observation
        try
            [wm,vm,ts] = imu_compensation (dataIMU(i:i+reverse,1:8),ins,reverse);
        catch ME
            disp(i);
            disp(ME.message);
        end
        % INS Update
%         insold  = ins;
        ins = insupdate(ins, wm,vm,ts);
        
%         qnb_1 = insold.qnb;vn_1 = insold.vn ;pos_1 = insold.pos;
%         [qnb2, vn2, pos2] = sins_forward(qnb_1, vn_1, pos_1, wm, vm, ts);
%         ins.qnb = qnb2;
%         ins.vn = vn2;
%         ins.pos = pos2;

%         diff1 = qq2phi(qnb2,ins.qnb);
%         diff2 = vn2-ins.vn;
%         diff3 = pos2 - ins.pos;

        %         kf.Phikk_1 = kffk(ins);
        kf.Phikk_1 = get_Fk(ins,method_id);
%         kf.Phikk_1 = get_Fk2(ins);
        kf = kfupdate(kf);
        % Get GNSS EKF Tag
        if i ~=idx_end
            [tag_ekf,dt,gnss_idx] = ekf_tag(dataIMU(i:i+reverse,1),ts0,dataGNSS.epoch,fb_flag);
        end
        % GPS EKF
        if tag_ekf == 1
            switch kf_obs
                case {'POS'}
                    % Interpolation of imu position and velocity at dataGNSS(gnns_idx,1)
                    %             [pos_tkgnss,vn_tkgnss] = learp_imu(x1,x2,ts,dt,fb_flag),
                    pos_tkgps = ins.pos - ins.Mpvvn*dt;
                    %              zk = [ins.vnL-ins.an*dt; ins.posL-ins.Mpvvn*dt]-vpGPS(kgps,1:6)';
                    % Get GPS position with lever arm compensation
                    posGNSS = dataGNSS.pos_vel(gnss_idx,1:3)';
                    posGNSS = la_compensation(ins,lever,posGNSS);
                    zk = pos_tkgps-posGNSS;
                    obs_err(i,1:3) = zk;
                    kf.Rk = get_R(dataGNSS.pos_std(gnss_idx,:));
                case {'POS+VEL'}
                    %....
            end
            kf = kfupdate(kf, zk, 'M');
            %             obs_err =;
            %             [kf, ins] = kffeedback(kf, ins, kf_flag );
        end
        %         [kf, ins] = kffeedback(kf, ins, kf_flag );
        [kf, ins] = kf_feedback(kf, ins, method_id );
        
        t = dataIMU(i,1); % t = dataIMU(i+reverse,1);
        dataXk(i,:) = [t;ins.avp; ins.eb; ins.db; ins.Kg ;ins.Ka ]';
        dataPk(i,:) = [t; diag(kf.Pxk)]';
    end
elseif strcmp(fb_flag,'backward')
    % Direction:backward
    
end
disp('Loose coupled function done');
