function [qnb, vn, pos] = sins_back(qnb_1, vn_1, pos_1, wm, vm, ts)
% wm,vm are using two continuous epochs(k-1,k), 3*2
%
%Update SINS information with measurement, we add the rotational and
%sculling motion, and coning error based on EH SHIN thesis and use Yan's
%program to compute

% % %*****************************************************
% % %***************   OLD VERSION BEGIN *****************
% % %*** It works, but the speed is slow *****************
% tss = ts;
% phim = wm(:,2)+cross(wm(:,1),wm(:,2))/12;
% dvbm = vm(:,2)+0.5*cross(wm(:,2),vm(:,2))+(cross(wm(:,1),vm(:,2))+cross(vm(:,1),wm(:,2)))/12;
%  
% [wnie, wnen, gn, retp] = earth(pos_1, vn_1);
% wnie = -wnie;
% wnin = wnie+wnen;
% 
% % Attitude update
% qnb = qmul(qnb_1, rv2q(phim - qmulv(qconj(qnb_1),wnin*tss)));
% e_q = 0.5*(qnb'*qnb- 1);
% qnb = (1-e_q)*qnb;
%  
% % Velocity update
% vn = vn_1 + qmulv(rv2q(-wnin*(1.0/2*tss)),qmulv(qnb_1,dvbm)) ...
%     + (gn-cross(wnie+wnin,vn_1))*tss;% <SINS book> page 227 (11.72) or Qin book (9.6.6) or reference <Algorithm design and simulation for SINS based on Matlab> (7) Yan Gogmin
% vn1_1 = (vn+vn_1)/2;
% 
% % Position update,  Qin book chapter 9.7.3
% 
% pos = pos_1 + tss*[vn1_1(2)/retp.rmh;vn1_1(1)/(retp.rnh*retp.cl);vn1_1(3)];
% % %***************   OLD VERSION END   *****************
% % %*****************************************************


% %*****************************************************
% %***************   NEW VERSION BEGIN *****************
% %*** the input parameter of cross function is not ****
% %*** checked, to improve speed only  *****************
% phim = wm(:,2)+cross_jun(wm(:,1),wm(:,2))/12;
% dvbm = vm(:,2)+0.5*cross_jun(wm(:,2),vm(:,2))+(cross_jun(wm(:,1),vm(:,2))+cross_jun(vm(:,1),wm(:,2)))/12;
%  
% [wnie, wnen, gn, retp] = earth(pos_1, vn_1);
% wnie = -wnie;
% wnin = wnie+wnen;
% 
% % Attitude update
% qnb = qmul(qnb_1, rv2q(phim - qmulv(qconj(qnb_1),wnin*ts)));
% e_q = 0.5*(qnb'*qnb- 1);
% qnb = (1-e_q)*qnb;
%  
% % Velocity update
% vn = vn_1 + qmulv(rv2q(-wnin*(1.0/2*ts)),qmulv(qnb_1,dvbm)) ...
%     + (gn-cross_jun(wnie+wnin,vn_1))*ts;% <SINS book> page 227 (11.72) or Qin book (9.6.6) or reference <Algorithm design and simulation for SINS based on Matlab> (7) Yan Gogmin
% vn1_1 = (vn+vn_1)/2;
% 
% % Position update,  Qin book chapter 9.7.3
% 
% pos = pos_1 + ts*[vn1_1(2)/retp.rmh;vn1_1(1)/(retp.rnh*retp.cl);vn1_1(3)];

%*****************************************************
%***************   NEW VERSION 2   *****************
%*** the input parameter of cross function is not ****
%*** checked, to improve speed only  *****************

glv_f = 1/298.257;
glv_wie = 7.2921151467e-5;
glv_Re = 6378137;
glv_g0 = 9.7803267714;
glv_e = sqrt(2*glv_f-glv_f^2);
glv_e2 = glv_e^2;

% phim = wm(:,2)+cross_jun(wm(:,1),wm(:,2))/12;
% dvbm = vm(:,2)+0.5*cross_jun(wm(:,2),vm(:,2))+(cross_jun(wm(:,1),vm(:,2))+cross_jun(vm(:,1),wm(:,2)))/12;
temp1 = cross_jun(wm(:,1),wm(:,2));
temp2 = cross_jun(wm(:,2),vm(:,2));
temp3 = cross_jun(wm(:,1),vm(:,2));
temp4 = cross_jun(vm(:,1),wm(:,2));
temp5 = temp3+temp4; 
phim = wm(:,2)+temp1/12;
dvbm = vm(:,2)+temp2*0.5+temp5/12;

% [wnie, wnen, gn, retp] = earth(pos_1, vn_1);
% wnie = -wnie;
% wnin = wnie+wnen;
%----------------------------
sl=sin(pos_1(1)); cl=cos(pos_1(1)); tl=sl/cl; sl2=sl*sl; sl4=sl2*sl2;
wnie = glv_wie*[0; cl; sl];
sq = 1-glv_e2*sl2; sq2 = sqrt(sq);
RMh = glv_Re*(1-glv_e2)/sq/sq2+pos_1(3);
RNh = glv_Re/sq2+pos_1(3);
wnen = [-vn_1(2)/RMh; vn_1(1)/RNh; vn_1(1)/RNh*tl];
g = glv_g0*(1+5.27094e-3*sl2+2.32718e-5*sl4)-3.086e-6*pos_1(3); %  grs80 ellipsoid, normal gravity, page 56 <SINS>(3.89)
gn = [0;0;-g];
retp_cl = cl;retp_rmh = RMh;retp_rnh = RNh;
wnie = -wnie;
wnin = wnie+wnen;
%-----------------------------

% Attitude update
% qnb = qmul(qnb_1, rv2q(phim - qmulv(qconj(qnb_1),wnin*ts)));
temp1 = qconj(qnb_1);
temp2 = wnin*ts;
temp3 = qmulv(temp1,temp2);
temp4 = phim-temp3;
temp5 = rv2q(temp4);
qnb   = qmul(qnb_1,temp5);

e_q = 0.5*(qnb'*qnb- 1);
qnb = (1-e_q)*qnb;
 
% Velocity update
% vn = vn_1 + qmulv(rv2q(-wnin*(1.0/2*ts)),qmulv(qnb_1,dvbm)) ...
%     + (gn-cross_jun(wnie+wnin,vn_1))*ts;% <SINS book> page 227 (11.72) or Qin book (9.6.6) or reference <Algorithm design and simulation for SINS based on Matlab> (7) Yan Gogmin
temp1 = -wnin*0.5*ts;
temp2 = rv2q(temp1);
temp3 = qmulv(qnb_1,dvbm);
temp4 = qmulv(temp2,temp3);
temp5 = wnie+wnin;
temp6 = cross_jun(temp5,vn_1);
temp7 = (gn-temp6)*ts;
vn    = vn_1+temp4+temp7;

vn1_1 = (vn+vn_1)/2;

% Position update,  Qin book chapter 9.7.3

pos = pos_1 + ts*[vn1_1(2)/retp_rmh;vn1_1(1)/(retp_rnh*retp_cl);vn1_1(3)];


%****************  Potential **********************
% Attitude

% Position

% Velocity



%********************************************************



function c = cross_jun(a,b)
% ATTENTION: a, b must be the same dimensional vector !
% Calculate cross product
% c = [a(2,:).*b(3,:)-a(3,:).*b(2,:)
%      a(3,:).*b(1,:)-a(1,:).*b(3,:)
%      a(1,:).*b(2,:)-a(2,:).*b(1,:)];
 
 c = [a(2,1).*b(3,1)-a(3,1).*b(2,1)
     a(3,1).*b(1,1)-a(1,1).*b(3,1)
     a(1,1).*b(2,1)-a(2,1).*b(1,1)];
%***************   NEW VERSION END   *****************
%*****************************************************