function kf = kf_initial(ins,sLC)
glvs;
af_flag = sLC.af_flag;
method_id = sLC.METHOD_ID;
obs_id = sLC.OBS_ID ;
sImu = sLC.sImu;
interval = 1/sImu.IMU_HERTZ;

if strcmp (method_id,'EXP1')
    if strcmp(af_flag,'filter')
        kf.Pxk = diag([[0.02;0.02;0.03]*glv.deg; [.01;.01;.01]; [0.2/glv.Re;0.2/glv.Re;0.2]; [0.1;0.1;0.1]*glv.dph; [0.3;0.3;0.3]*glv.mg;[100;100;100]*glv.ppm;[300;300;300]*glv.ppm])^2;
    elseif strcmp(af_flag,'align')
        kf.Pxk = diag([[10;10;20]*glv.deg; [2;2;2]; [3/glv.Re;3/glv.Re;3]; [1;1;1]*glv.dph; [3;3;3]*glv.mg;[100;100;100]*glv.ppm;[300;300;300]*glv.ppm])^2;
    end
    kf.xk = zeros(21,1);
    kf.n = 21;
    if strcmp(obs_id,'POS')
        kf.Rk = zeros(3,3);
    end
    kf.Qk = get_Qk(sImu,interval,method_id);
    %      kf.Qt = diag([imuerr.web; imuerr.wdb; zeros(9+3,1)])^2;
    kf.Hk = get_Hk(method_id,obs_id);
end
kf.s = 1;
kf.xtau = ones(size(kf.xk))*eps;
kf.T_fb = 1;
kf.xfb = zeros(kf.n, 1);
%     kf.coef_fb = (1.0-exp(-kf.T_fb./kf.xtau));
%     kf.coef_fb = ar1coefs(kf.T_fb, kf.xtau);
xtau = kf.xtau;
xtau(kf.xtau<kf.T_fb) = kf.T_fb;
kf.coef_fb = kf.T_fb./xtau;   