% function rsgps_local_test_2(MISSION)
%*******************************************************************
%**************************   Modification *************************
%*******************************************************************

clear;
SESSION = '020_001767';
%
%   (1) Initialization
[sNpos,sFile,sImu,sRtklib,sLaMa,sBase,sIgs,sQmsg1] = npos_initialization(SESSION);

%   (2) Preparing data
%   (2-1) IMU data 
dataIMU = read_imu(sFile.imu);
% IMU body frame transform:
%   (2-2) IMU data quality
[dataIMU,sQmsg2] = qcheck_imu(dataIMU,sImu); 
 
%   (3) Processing GNSS
%   (3-1) Preparing GNSS files
 
%   (3-2) Forward double difference carrier phase process

% for i = base_order:base_order
%     base_name = Struct_BASE(i).NAME;
%     base_o_file = Struct_BASE(i).OFILE;
%     switch DIRECTION
%         case 'FORWARD'
%             rtklib_cfg_mission_file = Struct_BASE(i).RTKLIB_F_CFG;
%         case 'BACKWARD'
%             rtklib_cfg_mission_file = Struct_BASE(i).RTKLIB_B_CFG;
%     end
%     rtklib_pos_file = strcat(DIR_result,MISSION,'_',rtklib_id,'_',base_name,'.pos');
%     if  strcmp(product_id,'COD')||strcmp(product_id,'IGS')
%         rtklib_dos_cmd=strcat('rnx2rtkp -k ',{' '},rtklib_cfg_mission_file,' -o ',{' '},rtklib_pos_file,{' '}, ...
%             rover_o_file,{' '},base_o_file,{' '},rover_n_file,{' '},eph_file_1,{' '},eph_file_2,{' '},eph_file_3,{' '},erp_file);
%     elseif  strcmp(product_id,'BRDC')
%         rtklib_dos_cmd=strcat('rnx2rtkp -k ',{' '},rtklib_cfg_mission_file,' -o ',{' '},rtklib_pos_file,{' '}, ...
%             rover_o_file,{' '},base_o_file,{' '},rover_n_file,{' '},rover_g_file,{' '});
%     end
%     fprintf(1,'The RTKLIB is processing baseline %d , with BASE :%s !\n',i,base_name);
%     fprintf(1,'PLEASE waiting.... \n');
%     tic;
%     dos(char(rtklib_dos_cmd));
%     t_cost_rtklib = toc;
%     fprintf(1,'The process is completed within %d seconds. \n',t_cost_rtklib);
%     Struct_BASE(i).RTKLIB_POS_FILE = rtklib_pos_file;
% end

%   (3-3) Reverse double difference carrier phase process

%   (3-4) Quality check and fusion of forward/reverse solution


%   (3-5) Read POSGNSS result

% % ----------------------------------------------
% [pos_vel_array,pos_std_array,vel_std_array] = rtklib2data (gnss_file);
% data_gnss = [pos_vel_array,pos_std_array,vel_std_array];
% % -----------------------------------------------

% --------------------------------------- 
[pos_vel,pos_std,vel_std] = posgnss2data (sFile.cmb);
dataGNSS = [pos_vel,pos_std,vel_std];
% ----------------------------------------
% switch upper(rtklib_id)
%     case 'POSGNSS'
%         disp('From POSGNSS .cmb solution...');
%         gnss_file = Struct_FILE.POSPAC_CMB;
%         [pos_vel_array,pos_std_array,vel_std_array] = posgnss2data (gnss_file);
%         data_gnss = [pos_vel_array,pos_std_array,vel_std_array];
%     otherwise
%         disp('From RTKLIB .pos solution...');
%         gnss_file = strcat(Struct_DIR.RTKLIB_S,MISSION,'_',rtklib_id,'.pos');
%         [pos_vel_array,pos_std_array,vel_std_array] = rtklib2data (gnss_file);
%         data_gnss = [pos_vel_array,pos_std_array,vel_std_array];
% end

%   (4) Processing PSINS

%   (4-1) Forward GPS/PSINS
[xpk
%   (4-2) Reverse GPS/PSINS

%   (4-3) NPOS_avp fusion

%   (5) Processing quality check
%
%   (5-1) IMU data
%   (5-2) GNSS data
%   (5-3) GNSS RMS data
%   (5-4) PSINS/GNSS RMS data
%   (5-5) Forward/Reverse Difference test
%
%
%   (6) Processing external quality check with POSPAC SBET

%   (7) Displaying result


%%
%**************************************************************************
%**************************************************************************
% Struct_mission_cfg = rsgps_Struct_cfg_initial(SESSION);
% subtask_num = Struct_mission_cfg.Struct_GENERAL_CFG.SUBTASK_NUM;
% base_number = length(Struct_mission_cfg.Struct_BASE);
% %
% for i = 1:base_number
%     base_order = i;
%     %  (2) RTKLIB forward
%     rsgps_rtklib_init (SESSION,base_order);
%     
%     %  (3) RTKLIB backward, [(2) and (3) can run as parallelization]
%     rsgps_rtklib_end (SESSION,base_order);
% end
% 
% % (4) RTKLIB Quality Check (Selection of good baseline results)
% 
rsgps_rtklib_smooth (SESSION,0); % need to make some changes
% rsgps_qcheck_rtklib (SESSION);
% 
% Struct_Base_selection = rsgps_qcheck_rtklib (SESSION);
% % 
% Struct_Base_selection = {'5337','INJN'};
% % %  (5) RTKLIB Smooth, [have to run after (2) and (3) finish]
% rsgps_rtklib_smooth (SESSION,1,Struct_Base_selection); % need to make some changes
%
% 
%  
Struct_mission_cfg = rsgps_Struct_cfg_initial(SESSION);
for i = 1:Struct_mission_cfg.Struct_GENERAL_CFG.SUBTASK_NUM
    fprintf(1,'The %d subtask is processing...\n',i);
    sub_task_order = i;
    %  (6) Alignment forward
    rsgps_align_init(SESSION,sub_task_order);
%     %  (7) Alignment backward, [(5) and (6) can run as parallelization]
    rsgps_align_end(SESSION,sub_task_order);
%     
%     %  (8) Filter forward
    rsgps_filter_init (SESSION,sub_task_order);
%     %  (9) Filter backward,  [ (7) and (8) can run as parallelization]
    rsgps_filter_end (SESSION,sub_task_order);
    
    %  (10) SBET generation
    rsgps_sbet_generation(SESSION,sub_task_order);
end

%         %  (11) SBET Combination
rsgps_sbet_combination(SESSION);

%  (12) Qualtiy report
rsgps_qcheck(SESSION);
% 
% %  (13) External Quality report
% % rsgps_qcheck_sbet(SESSION);
% 
% fprintf(1,'SESSION :%s completed !\n',SESSION);