function    [phim, dvbm] = imu_coning_sculling(wm,vm) 
% Program: 
%   Compute the sculling and coning correction, reference <Eun-Hwan
%   Shin(2006) - Estimation Techniques for Low-Cost Inertial Navigation>
%   section 2.3.3 to section 2.3.5
% Input:
%   wm - gyro angular increments
%   vm - acc velocity increments 
% Output:
%   phim - rotation vector after coning compensation
%	dvbm - velocity increment after rotation & sculling compensation

temp1 = cross_fast(wm(:,1),wm(:,2));
temp2 = cross_fast(wm(:,2),vm(:,2));
temp3 = cross_fast(wm(:,1),vm(:,2));
temp4 = cross_fast(vm(:,1),wm(:,2));
temp5 = temp3+temp4; 

phim = wm(:,2)+temp1/12; % coning correction
dvbm = vm(:,2)+temp2*0.5+temp5/12; % sculling correction

% phim = wm(:,1)+temp1/12; % coning correction
% dvbm = vm(:,1)+temp2*0.5+temp5/12; % sculling correction

% phim = wm(:,2)+cross_jun(wm(:,1),wm(:,2))/12;
% dvbm = vm(:,2)+0.5*cross_jun(wm(:,2),vm(:,2))+(cross_jun(wm(:,1),vm(:,2))+cross_jun(vm(:,1),wm(:,2)))/12;
 