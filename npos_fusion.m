function [dataXk,dataRMS ] = npos_fusion(dataXk1,dataRMS1,dataXk2,dataRMS2) 

% POS data fusion for forward and backward results.
% Default : attitude,velocity,position

[t, i1, i2] = intersect(dataXk1(:,1), dataXk2(:,1));
n = length(t);
x1 = dataXk1(i1,2:10);  p1 = dataRMS1(i1,2:10);
x2 = dataXk2(i2,2:10);  p2 = dataRMS2(i2,2:10);
  
x = zeros(n,10);
p = zeros(n,10);
for k=1:length(t)
    [x(k,2:10), p(k,2:10)] = fusion(x1(k,:), p1(k,:), x2(k,:), p2(k,:));
    att = attfusion(x1(k,1:3), x2(k,1:3), p1(k,1:3), p2(k,1:3));
    x(k,2:4) = att;
end
x(:,1) = dataXk1(i1,1);
p(:,1) = x(:,1);
dataXk = x;
dataRMS = p;

function att = attfusion(att1, att2, p1, p2)
att = att1;
for k=1:size(att1, 1)
    pf = p1(k,:)./(p1(k,:)+p2(k,:));
    q1k = a2qua(att1(k,:)');
    phi = qq2phi(a2qua(att2(k,:)'), q1k);
    att(k,:) = q2att( qaddphi(q1k,phi.*pf') );
end
