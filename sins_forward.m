function [qnb, vn, pos] = sins_forward(qnb_1, vn_1, pos_1, wm, vm, ts)
 
glv_f = 1/298.257;
glv_wie = 7.2921151467e-5;
glv_Re = 6378137;
glv_g0 = 9.7803267714;
glv_e = sqrt(2*glv_f-glv_f^2);
glv_e2 = glv_e^2;
 
temp1 = cross_fast(wm(:,1),wm(:,2));
temp2 = cross_fast(wm(:,2),vm(:,2));
temp3 = cross_fast(wm(:,1),vm(:,2));
temp4 = cross_fast(vm(:,1),wm(:,2));
temp5 = temp3+temp4; 
phim = wm(:,2)+temp1/12;
dvbm = vm(:,2)+temp2*0.5+temp5/12;
% phim = wm(:,2)+cross_jun(wm(:,1),wm(:,2))/12;
% dvbm = vm(:,2)+0.5*cross_jun(wm(:,2),vm(:,2))+(cross_jun(wm(:,1),vm(:,2))+cross_jun(vm(:,1),wm(:,2)))/12;
 
% [wnie, wnen, gn, retp] = earth(pos_1, vn_1);
%----------------------
sl=sin(pos_1(1)); cl=cos(pos_1(1)); tl=sl/cl; sl2=sl*sl; sl4=sl2*sl2;
wnie = glv_wie*[0; cl; sl];
sq = 1-glv_e2*sl2; sq2 = sqrt(sq);
RMh = glv_Re*(1-glv_e2)/sq/sq2+pos_1(3);
RNh = glv_Re/sq2+pos_1(3);
wnen = [-vn_1(2)/RMh; vn_1(1)/RNh; vn_1(1)/RNh*tl];
g = glv_g0*(1+5.27094e-3*sl2+2.32718e-5*sl4)-3.086e-6*pos_1(3); %  grs80 ellipsoid, normal gravity, page 56 <SINS>(3.89)
gn = [0;0;-g];
retp_cl = cl;retp_rmh = RMh;retp_rnh = RNh;
%---------------------

% vn = vn_1 + qmulv(rv2q(-wnin*(1.0/2*ts)),qmulv(qnb_1,dvbm)) + (gn-cross_jun(wnie+wnin,vn_1))*ts; 
%---------------------------
wnin = wnie+wnen;
temp1 = rv2q(-wnin*(1.0/2*ts));
temp2 = qmulv(qnb_1,dvbm);
temp3 = cross_fast(wnie+wnin,vn_1);
temp4 = qmulv(temp1,temp2);
temp5 = (gn-temp3)*ts;
vn = vn_1+temp4+temp5;
%----------------------------
vn1_1 = (vn+vn_1)*0.5;

% Position update,  Qin book chapter 9.7.3

pos = pos_1 + ts*[vn1_1(2)/retp_rmh;vn1_1(1)/(retp_rnh*retp_cl);vn1_1(3)];

% Attitude update 
% wnin = earth_wnin(0.5*(pos+pos_1), vn1_1);
%------------------------------
postemp = 0.5*(pos+pos_1);
sl=sin(postemp(1)); cl=cos(postemp(1)); tl=sl/cl; sl2=sl*sl; sl4=sl2*sl2;
wnie = [0; glv_wie*cl; glv_wie*sl];
sq = 1-glv_e2*sl2; sq2 = sqrt(sq);
RMh = glv_Re*(1-glv_e2)/sq/sq2+postemp(3);
RNh = glv_Re/sq2+postemp(3);
wnen = [-vn1_1(2)/RMh; vn1_1(1)/RNh; vn1_1(1)/RNh*tl];
wnin = wnie+wnen;
%------------------------------



% qnb = qmul(qnb_1, rv2q(phim - qmulv(qconj(qnb_1),wnin*ts)));
%----------------------------
temp1 = qmulv(qconj(qnb_1),wnin*ts);
temp2 = rv2q(phim-temp1);
qnb = qmul(qnb_1,temp2);
%-----------------------------

e_q = 0.5*(qnb'*qnb- 1);
qnb = (1-e_q)*qnb;


function c = cross_fast(a,b) 
 c = [a(2,1).*b(3,1)-a(3,1).*b(2,1)
     a(3,1).*b(1,1)-a(1,1).*b(3,1)
     a(1,1).*b(2,1)-a(2,1).*b(1,1)];
%***************   NEW VERSION END   *****************
%*****************************************************
