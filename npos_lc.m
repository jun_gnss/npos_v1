function [dataXk,dataPk,ins] = npos_lc(dataIMU,dataGNSS,ins,sLC )

% Program: npos_lc - NPOS Loose Coupled, using the IMU data and GNSS
%   position result to get SBET data and RMS
%
% Input:
%   dataIMU - IMU data n*7 [t,wx,wy,wz,ax,ay,az]
%   dataGNSS - structure GNSS result
%   sAlign - struct of IMU initial information as well as lever arm
%   sLC - struct of Loose Coulped,
%
% Output:

% (1) Time synchronization
af_flag = sLC.af_flag; %Loose couple for aligning or filtering
fb_flag = sLC.fb_flag;
method_id = sLC.METHOD_ID;
ts0 = 1/sLC.IMU_HERTZ;
kf_obs = sLC.OBS_ID;
lever = sLC.sLaMa.LEVER_ARM' ;
t_start = ins.time;
glvs;
% Initialization based on af_flag and fb_flag

[~,idx_start] = find_close_epoch(dataIMU(:,1),t_start);
reverse = reverse_flag(fb_flag);
%
if strcmp(fb_flag,'forward')
    idx_end = size(dataIMU,1)-1;
elseif strcmp(fb_flag,'backward')
    idx_end = 2;
    ins.vn = -ins.vn; ins.eb = -ins.eb;
end
len = abs(idx_start-idx_end)+1;

if strcmp(method_id,'EXP1')
    Xk = zeros(21,1);
    dataXk = zeros(len,10);
    dataPk = zeros(len,10);
    obs_err = zeros(len,3);
    [eb, db,Kg, Ka] = setvals(zeros(3,1));
end

% (2) Filtering
qnb = ins.qnb; vn = ins.vn; pos = ins.pos;
qnb_old = qnb;  vn_old = vn;   pos_old = pos;
qnb_new = qnb;  vn_new = vn;   pos_new = pos;
Pxk = get_Pxk(method_id,af_flag);
kf_num = 0;
if strcmp(fb_flag,'forward')
    k = 0;
    for i = idx_start:reverse:idx_end
        k = k+1;
        disp_progress(i,60000,len,fb_flag);
        [wm,vm,ts] = imu_compensation (dataIMU(i:i+reverse,1:8),ins,reverse);
        % Get GNSS EKF Tag
        [tag_ekf,dt,gnss_idx] = ekf_tag(dataIMU(i:i+reverse,1),ts0,dataGNSS.epoch,fb_flag);
        
        if tag_ekf == 1
            kf_num = kf_num+1;
            pos_tkgps = interp1([0 ts],[pos_old pos_new]',dt,'linear','extrap')';
            vn_tkgps = interp1([0 ts],[vn_old vn_new]',dt,'linear','extrap')';
            posGNSS = dataGNSS.pos_vel(gnss_idx,1:3)';
            [posIMU] = la_compensation(qnb,lever,pos_tkgps,vn_tkgps,posGNSS);
            Ft = get_Ft(qnb_new, vn_new, pos_new, vm(:,2)/ts, wm(:,2)/ts,method_id,'forward');
            [Fikk_1, Qk] = get_Fk_Qk(Ft,ts,method_id);
            Zk = pos_tkgps-posIMU;
            obs_err(kf_num,1:3) = Zk;
            Rk = get_R(dataGNSS.pos_std(gnss_idx,:));
            Hk = get_Hk(method_id,kf_obs);
            Pxk = diag_posdef(Pxk);
            [Xk, Pxk] = kalman(Fikk_1, Qk, Xk, Pxk, Hk, Rk, Zk);
            pos(3) = pos_new(3)-Xk(9,1);
            pos(2) = pos_new(2)-Xk(8,1);
            pos(1) = pos_new(1)-Xk(7,1);
            vn(3) = vn_new(3)-Xk(6,1);
            vn(2) = vn_new(2)-Xk(5,1);
            vn(1) = vn_new(1)-Xk(4,1);
            qnb = qdelphi(qnb_new, Xk(1:3,1));
            eb = eb - Xk(10:12);
            db = db - Xk(13:15);
            Kg = Kg-Xk(16:18);
            Ka = Ka-Xk(19:21);
            ins.Kg = Kg;
            ins.eb = eb;
            ins.Ka = Ka;
            ins.db = db;
            Xk(1:21,1) = 0;
            
            qnb_old = qnb;
            vn_old = vn;
            pos_old = pos;
            [qnb,vn,pos] = sins_forward(qnb_old,vn_old,pos_old,wm,vm,ts);
            qnb_new = qnb;
            vn_new = vn;
            pos_new = pos;
            
            Ft = get_Ft(qnb_new, vn_new, pos_new, vm(:,2)/ts, wm(:,2)/ts,method_id,fb_flag);
            [Fikk_1, Qk] = get_Fk_Qk(Ft,ts,method_id);
            Pxk = diag_posdef(Pxk);
            [Xk, Pxk] = kalman(Fikk_1, Qk, Xk, Pxk );
        else
            qnb_old = qnb;
            vn_old = vn;
            pos_old = pos;
            [qnb,vn,pos] = sins_forward(qnb,vn,pos,wm,vm,ts);
            qnb_new = qnb;
            vn_new = vn;
            pos_new = pos;
            
            Ft = get_Ft(qnb_new, vn_new, pos_new, vm(:,2)/ts, wm(:,2)/ts,method_id,fb_flag);
            [Fikk_1, Qk] = get_Fk_Qk(Ft,ts,method_id);
            %             [Xk, Pxk] = kfupdate2(Fikk_1, Qk, Xk, Pxk);
            Pxk = diag_posdef(Pxk);
            [Xk, Pxk] = kalman(Fikk_1, Qk, Xk, Pxk );
        end
        dataXk(k,1) = dataIMU(i+1,1);
        dataXk(k,2:4) = q2att(qnb);
        dataXk(k,5:7) = vn;
        dataXk(k,8:10) = pos;
        % For smoothing
        dataPk(k,1) = dataIMU(i+1,1);
        dataPk(k,2:10) = [Pxk(1,1),Pxk(2,2),Pxk(3,3),Pxk(4,4),Pxk(5,5),Pxk(6,6),Pxk(7,7),Pxk(8,8),Pxk(9,9)];
    end
    ins.qnb = qnb;
    ins.vn = vn;
    ins.pos = pos;
    ins.time = dataIMU(i+1,1);
    return;
    %     var_Xk_filter(1,:) = var_Xk_filter(2,:);
end
%%--- Backward----
if strcmp(fb_flag,'backward')
    k = len+1;
    for i = idx_start:reverse:idx_end
        k = k-1;
        disp_progress(i,60000,len,fb_flag);
        if i == 21700
            disp('debuging');
        end
        [wm,vm,ts] = imu_compensation (dataIMU(i:reverse:i+reverse,1:8),ins,reverse);
        % Get GNSS EKF Tag
        if i ~=idx_end
            [tag_ekf,dt,gnss_idx] = ekf_tag(dataIMU(i:reverse:i+reverse,1),ts0,dataGNSS.epoch,fb_flag);
        end
        if isnan (pos)
            disp(i);
        end
        if tag_ekf == 1
            kf_num = kf_num+1;
            %                 pos_tkgps = interp1([0 ts],[pos_old pos_new]',dt,'linear','extrap')';
            %                 vn_tkgps = interp1([0 ts],[vn_old vn_new]',dt,'linear','extrap')';
            try
                pos_tkgps = interp1([ ts,0 ],[pos_old pos_new]',dt,'linear')';
                vn_tkgps = interp1([ ts,0],[vn_old vn_new]',dt,'linear')';
 
                %                 pos_tkgps = interp1([0 ts],[pos_old pos_new]',dt,'linear','extrap')';
                %                 vn_tkgps = interp1([0 ts],[vn_old vn_new]',dt,'linear','extrap')';
            catch
                disp(i);
                disp(dt);
            end
            
            posGNSS = dataGNSS.pos_vel(gnss_idx,1:3)';
            [posIMU] = la_compensation(qnb,lever,pos_tkgps,vn_tkgps,posGNSS);
            Ft = get_Ft(qnb_new, vn_new, pos_new, vm(:,2)/ts, wm(:,2)/ts,method_id,fb_flag);
            [Fikk_1, Qk] = get_Fk_Qk(Ft,ts,method_id);
            Zk = pos_tkgps-posIMU;
            obs_err(kf_num,1:3) = Zk;
            Rk = get_R(dataGNSS.pos_std(gnss_idx,:));
            Hk = get_Hk(method_id,kf_obs);
            %             [Xk, Pxk] = kfupdate2(Fikk_1, Qk, Xk, Pxk, Hk, Rk, Zk);
            Pxk = diag_posdef(Pxk);
            try
                [Xk, Pxk] = kalman(Fikk_1, Qk, Xk, Pxk, Hk, Rk, Zk);
            catch ME
                disp(Zk);
                disp(i);
            end
            pos(3) = pos_new(3)-Xk(9,1);
            pos(2) = pos_new(2)-Xk(8,1);
            pos(1) = pos_new(1)-Xk(7,1);
            vn(3) = vn_new(3)-Xk(6,1);
            vn(2) = vn_new(2)-Xk(5,1);
            vn(1) = vn_new(1)-Xk(4,1);
            qnb = qdelphi(qnb_new, Xk(1:3,1));
            eb = eb - Xk(10:12);
            db = db - Xk(13:15);
            Kg = Kg-Xk(16:18);
            Ka = Ka-Xk(19:21);
            ins.Kg = Kg;
            ins.eb = eb;
            ins.Ka = Ka;
            ins.db = db;
            Xk(1:21,1) = 0;
            
            qnb_old = qnb;
            vn_old = vn;
            pos_old = pos;
            [qnb,vn,pos] = sins_back(qnb_old,vn_old,pos_old,wm,vm,ts);
            qnb_new = qnb;
            vn_new = vn;
            pos_new = pos;
            
            Ft = get_Ft(qnb_new, vn_new, pos_new, vm(:,2)/ts, wm(:,2)/ts,method_id,fb_flag);
            [Fikk_1, Qk] = get_Fk_Qk(Ft,ts,method_id);
            Pxk = diag_posdef(Pxk);
            [Xk, Pxk] = kalman(Fikk_1, Qk, Xk, Pxk);
            %             [Xk, Pxk] = kfupdate2(Fikk_1, Qk, Xk, Pxk);
        else
            qnb_old = qnb;
            vn_old = vn;
            pos_old = pos;
            [qnb,vn,pos] = sins_back(qnb,vn,pos,wm,vm,ts);
            qnb_new = qnb;
            vn_new = vn;
            pos_new = pos;
            
            Ft = get_Ft(qnb_new, vn_new, pos_new, vm(:,2)/ts, wm(:,2)/ts,method_id,fb_flag);
            [Fikk_1, Qk] = get_Fk_Qk(Ft,ts,method_id);
            
            %             [Xk, Pxk] = kfupdate2(Fikk_1, Qk, Xk, Pxk);
            Pxk = diag_posdef(Pxk);
            [Xk, Pxk] = kalman(Fikk_1, Qk, Xk, Pxk);
            
        end
        dataXk(k,1) = dataIMU(i+reverse,1);
        dataXk(k,2:4) = q2att(qnb); %  Attitude: Pitch, Roll, Yaw [rad/rad/rad]
        dataXk(k,5:7) = -vn; % Velocity: East,North, Up [meter/second]
        dataXk(k,8:10) = pos; % Position: latitude,longitude,height [rad/rad/meter]
        % For smoothing
        dataPk(k,1) = dataXk(i+reverse,1) ;
        dataPk(k,2:10) = [Pxk(1,1),Pxk(2,2),Pxk(3,3),Pxk(4,4),Pxk(5,5),Pxk(6,6),Pxk(7,7),Pxk(8,8),Pxk(9,9)];
    end
    t1 = dataXk(:,1);
    t2 = t1;
    t2(2:end) = t1(1:end-1);
    dataXk(:,1) = t2;
    ins.qnb = qnb;
    ins.vn = -vn;
    ins.pos = pos;
    ins.time = dataIMU(i+reverse,1);
    
end

disp('Loose coupled function done');
