% function [dataSBET,dataRMS] = npos_v1(SESSION)
SESSION = '13120803';

% SESSION = '13120901';

%   (1) Initialization 
addpath('I:\Program\NPOS\psins150315\base');
glvs;
fprintf(1,'NPOS Initialization starts...\n');
% [sFile,sNpos,sImu,sLaMa,sRtklib,sBase,sIgs,sQmsg1] = npos_initialization(SESSION);
[sFile,sNpos,sImu,sLaMa,~,~,~,~] = npos_initialization(SESSION);
% write_log(sFile,sQmsg1,step_id);
fprintf(1,'NPOS Initialization done!\n');

%   (2) Preparing data
fprintf(1,'IMU data loading and checking starts...\n');
%   (2-1) IMU data
dataIMU = read_imu(sFile.FILE_IMU);
% IMU body frame transform:
dataIMU(:,2:7) = imurfu(dataIMU(:,2:7), 'FRD');
%   (2-2) IMU data quality
imu_hz = sImu.IMU_HERTZ;
leap_sec = sNpos.LEAP_SECONDS;
[dataIMU_OLD,~] = qcheck_imu(dataIMU,imu_hz,leap_sec);
% delete some data
k1 = sNpos.SECONDS_DELETE_IMU*sImu.IMU_HERTZ;
k2 = k1+sNpos.MISSION_HOURS*3600*200;
if k2<length(dataIMU)
    dataIMU = dataIMU_OLD(k1:k2,:);
else
    dataIMU = dataIMU_OLD(k1:end,:);
end
% write_log(sFile,sQmsg2,step_id);
fprintf(1,'IMU data loading and checking done!\n');

%   (3) Processing GNSS
fprintf(1,'GNSS Calculation starts...\n');
% [dataGNSS,sQmsg3] = npos_gnss(sFile,sBase,sRtklib,sIgs);

% ---------------------------------------
[pos_vel,pos_std,vel_std] = posgnss2data (sFile.FILE_POSGNSS);
dataGNSS = convert2gnss_npos(pos_vel,pos_std,vel_std,sNpos);
% dataGNSS = gpstime_check(dataGNSS);
% ----------------------------------------
% write_log(sFile,sQmsg3,step_id);
fprintf(1,'GNSS Calculation done!\n');

%   (4) Processing PSINS
fprintf(1,'GNSS/IMU Loose Coulped Processing starts...\n');
sLC = npos_initLC(sNpos,sImu,sLaMa);
%   (4-1) Forward IMU/GNSS
fprintf(1,'Forward Filtering Processing...\n');
[dataXk_f,dataRMS_f,~] = npos_filter(dataIMU,dataGNSS,sLC,'forward');

%   (4-2) Reverse IMU/GNSS
fprintf(1,'Backward Filtering Processing...\n');
[dataXk_b,dataRMS_b,~] = npos_filter(dataIMU,dataGNSS,sLC,'backward');

%   (4-3) NPOS_avp fusion
%--------------
t1 = dataXk_b(:,1);
t2 = t1;
t2(2:end) = t1(1:end-1);
dataXk_b(:,1) = t2;
%--------------

[dataNpos,nposRMS] = npos_fusion(dataXk_f,dataRMS_f,dataXk_b,dataRMS_b);

% Transform to SBET
[dataSBET, sbetRMS ] = npos2sbet(dataNpos,nposRMS,sLC);

[sbet2 ] = npos2sbet(dataXk_b,dataRMS_b,sLC);
[sbet1 ] = npos2sbet(dataXk_f,dataRMS_f,sLC);

% write_sbet(dataSBET,sbetRMS,sFile,SESSION);

% write_log(sFile,sQmsg_f,step_id);
% write_log(sFile,sQmsg_b,step_id);
% write_log(sFile,sQmsg4,step_id);
fprintf(1,'GNSS/IMU Loose Coulped Processing done!\n');
%   (5) Processing quality check

%   (5-1) IMU data
%   (5-2) GNSS data
%   (5-3) GNSS RMS data
%   (5-4) PSINS/GNSS RMS data
%   (5-5) Forward/Reverse Difference test
%
%
%   (6) Processing external quality check with POSPAC SBET
datasbet2 = read_sbet(sFile.FILE_SBET);
cmp_result = cmp_2sbet_data(dataSBET,datasbet2);
cmp_resultf = cmp_2sbet_data(sbet1,datasbet2);
cmp_resultb = cmp_2sbet_data(sbet2,datasbet2);
%   (7) Displaying result
%   (7-1) Internal Quality Check
%   (7-2) External Quality Check