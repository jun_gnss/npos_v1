figure;
subplot(3,1,1);plot(cmp_result.diff_lat*glv.Re,'.');title('smooth latitude');ylim([-0.4,0.4]);
subplot(3,1,2);plot(cmp_resultf.diff_lat*glv.Re,'.');title('forward latitude');ylim([-0.4,0.4]);
subplot(3,1,3);plot(cmp_resultb.diff_lat*glv.Re,'.');title('backward latitude');ylim([-0.4,0.4]);

figure;
subplot(3,1,1);plot(cmp_result.diff_lon*glv.Re,'.');title('smooth longitude');ylim([-0.4,0.4]);
subplot(3,1,2);plot(cmp_resultf.diff_lon*glv.Re,'.');title('forward longitude');ylim([-0.4,0.4]);
subplot(3,1,3);plot(cmp_resultb.diff_lon*glv.Re,'.');title('backward longitude');ylim([-0.4,0.4]);

figure;
subplot(3,1,1);plot(cmp_result.diff_h,'.');title('smooth height');ylim([-0.1,0.1]);
subplot(3,1,2);plot(cmp_resultf.diff_h,'.');title('forward height');ylim([-0.1,0.1]);
subplot(3,1,3);plot(cmp_resultb.diff_h,'.');title('backward height');ylim([-0.1,0.1]);


figure;
subplot(3,1,1);plot(cmp_result.diff_pitch*180/pi,'.');title('smooth pitch');ylim([-0.02,0.02]);
subplot(3,1,2);plot(cmp_resultf.diff_pitch*180/pi,'.');title('forward pitch');ylim([-0.02,0.02]);
subplot(3,1,3);plot(cmp_resultb.diff_pitch*180/pi,'.');title('backward pitch');ylim([-0.02,0.02]);

figure;
subplot(3,1,1);plot(cmp_result.diff_roll*180/pi,'.');title('smooth roll');ylim([-0.02,0.02]);
subplot(3,1,2);plot(cmp_resultf.diff_roll*180/pi,'.');title('forward roll');ylim([-0.02,0.02]);
subplot(3,1,3);plot(cmp_resultb.diff_roll*180/pi,'.');title('backward roll');ylim([-0.02,0.02]);

figure;
subplot(3,1,1);plot(cmp_result.diff_yaw*180/pi,'.');title('smooth yaw');ylim([-0.04,0.04]);
subplot(3,1,2);plot(cmp_resultf.diff_yaw*180/pi,'.');title('forward yaw');ylim([-0.04,0.04]);
subplot(3,1,3);plot(cmp_resultb.diff_yaw*180/pi,'.');title('backward yaw');ylim([-0.04,0.04]);

figure;
subplot(3,1,1);plot(sqrt(nposRMS(:,8))*glv.Re,'.');title('latitude rms');ylim([0.005,0.01]);
subplot(3,1,2);plot(sqrt(nposRMS(:,9))*glv.Re,'.');title('longitude rms');ylim([0.005,0.01]);
subplot(3,1,3);plot(sqrt(nposRMS(:,10)),'.');title('height rms');ylim([0.005,0.02]);


%--------------
save('I:\Program\NPOS\Debug log\result\res13120803_v1.mat','dataNpos','dataRMS_f','dataRMS_b',...
    'dataXk_f','dataXk_b','nposRMS');