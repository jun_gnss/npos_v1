function [xk0,pk0] = kf_xk_initial(af_flag,xk_num,ins,sLC)

if xk_num == 21
    if strcmp(af_flag,'filter')
        pk0 = diag([[0.02;0.02;0.03]*glv.deg; [.01;.01;.01]; [0.2/glv.Re;0.2/glv.Re;0.2]; [0.1;0.1;0.1]*glv.dph; [0.3;0.3;0.3]*glv.mg;[100;100;100]*glv.ppm;[300;300;300]*glv.ppm])^2;
    elseif strcmp(af_flag,'align')
        pk0 = diag([[10;10;20]*glv.deg; [2;2;2]; [3/glv.Re;3/glv.Re;3]; [1;1;1]*glv.dph; [3;3;3]*glv.mg;[100;100;100]*glv.ppm;[300;300;300]*glv.ppm])^2;        
    end
    xk0 = zeros(21,1);
else
    error('The xk and pk information is not initialized correct!\n');
end


[Re,deg,dph,ug,mg] = ... % just for short
    setvals(glv.Re,glv.deg,glv.dph,glv.ug,glv.mg); 
o33 = zeros(3); I33 = eye(3); 
kf = [];
switch(psinsdef.kfinit)
    case psinsdef.kfinit153,
        psinsdef.kffk = 15;  psinsdef.kfhk = 153;  psinsdef.kfplot = 15;
        [davp, imuerr] = setvals(varargin);
        kf.Qt = diag([imuerr.web; imuerr.wdb; zeros(9,1)])^2;
        kf.Rk = diag(davp(7:9))^2;
        kf.Pxk = diag([davp; imuerr.eb; imuerr.db]*10)^2;
        kf.Hk = kfhk(0);
    case psinsdef.kfinit183,
        psinsdef.kffk = 18;  psinsdef.kfhk = 183;  psinsdef.kfplot = 18;
        [davp, imuerr, lever, r0] = setvals(varargin);
        kf.Qt = diag([imuerr.web; imuerr.wdb; zeros(9+3,1)])^2;
        kf.Rk = diag(r0)^2;
        kf.Pxk = diag([davp; imuerr.eb; imuerr.db; lever]*1.0)^2;
        kf.Hk = zeros(3,18);
    case psinsdef.kfinit186,
        psinsdef.kffk = 18;  psinsdef.kfhk = 186;  psinsdef.kfplot = 18;
        [davp, imuerr, lever, r0] = setvals(varargin);
        kf.Qt = diag([imuerr.web; imuerr.wdb; zeros(9+3,1)])^2;
        kf.Rk = diag(r0)^2;
        kf.Pxk = diag([davp; imuerr.eb; imuerr.db; lever]*1.0)^2;
        kf.Hk = zeros(3,18);
    case psinsdef.kfinit193,
        psinsdef.kffk = 19;  psinsdef.kfhk = 193;  psinsdef.kfplot = 19;
        [davp, imuerr, lever, dT, r0] = setvals(varargin);
        kf.Qt = diag([imuerr.web; imuerr.wdb; [1/Re;1/Re;1]*glv.mpsh; ...
            [1;1;1]*0.01*glv.dphpsh; [1;1;1]*10*glv.ugpsh; [1;1;1]*0.1*glv.mpsh; 0])^2;
        kf.Rk = diag(r0)^2;
        kf.Pxk = diag([davp; imuerr.eb; imuerr.db; lever; dT]*1.0)^2;
        kf.Hk = zeros(3,19);
    case psinsdef.kfinit343,
        psinsdef.kffk = 34;  psinsdef.kfhk = 343;  psinsdef.kfplot = 34;
        [ins, davp, imuerr, lever, dT] = setvals(varargin);
        kf.Qt = diag([imuerr.web; imuerr.wdb; zeros(9+3+1+15,1)])^2;
        kf.Rk = diag(davp(7:9))^2;
        kf.Pxk = diag([davp; imuerr.eb; imuerr.db; lever; dT; imuerr.dKga]*10)^2;
        kf.Hk = kfhk(ins);
        kf.xtau(1:psinsdef.kffk,1) = 0;
    case psinsdef.kfinit376,
        psinsdef.kffk = 37;  psinsdef.kfhk = 376;  psinsdef.kfplot = 37;
        [ins, davp, imuerr, lever, dT] = setvals(varargin);
        kf.Qt = diag([imuerr.web; imuerr.wdb; zeros(9+3+1+15+3,1)])^2;
        kf.Rk = diag(davp(4:9))^2;
        kf.Pxk = diag([davp; imuerr.eb; imuerr.db; lever; dT; imuerr.dKga; davp(4:6)]*10)^2;
        kf.Hk = kfhk(ins);
        kf.xtau(1:psinsdef.kffk,1) = 0;
    otherwise,
        kf = feval(psinsdef.typestr, psinsdef.kfinittag, [{nts},varargin]);
end
kf = kfinit0(kf, nts);