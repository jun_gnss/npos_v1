function     sLaMa = read_instl_cfg(file_instl,plane_id)

fid = fopen(file_instl);
sLaMa = struct('LEVER_ARM',[],'MOUNTING_ANGLE',[]);
while 1
    if (feof(fid)==1)
        break
    end
    lin = fgets(fid);
    if strfind(lin,'%')
        continue;
    end
    lin_cell =  textscan(lin,'%s %s');
    lin_flag = lin_cell{1}(1);
    switch char(lin_flag)
        case 'LEVER_ARM_020'
            lin_cell =  textscan(lin,'%s %c %f %f %f %c');
            la020 =  [lin_cell{3},lin_cell{4},lin_cell{5} ];
        case 'LEVER_ARM_023'
            lin_cell =  textscan(lin,'%s %c %f %f %f %c');
            la023 =   [lin_cell{3},lin_cell{4},lin_cell{5} ];
        case 'MOUNTING_ANGLE'
            lin_cell =  textscan(lin,'%s %c %f %f %f %c');
            ma =   [lin_cell{3},lin_cell{4},lin_cell{5} ];
    end
end
fclose(fid);
% Body frame transformation
la023 = imurfu(la023, 'FRD');
la020 = imurfu(la020, 'FRD');
ma =  imurfu(ma, 'FRD');
sLaMa.MOUNTING_ANGLE = ma;
if strcmp(plane_id,'023')
    sLaMa.LEVER_ARM = la023;
elseif strcmp(plane_id,'020')
    sLaMa.LEVER_ARM = la020;
end