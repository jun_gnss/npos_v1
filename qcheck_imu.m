function [imu_data_new,qcheck_msg,Struct_imu_correct] = qcheck_imu (imu_data_old,imu_hz,leap_sec)

% Version 1.0: 2013-10-10, only consider modifying the gps time which is
% not synchronized with GPS and the incorrect time stamp(Plane: '023')
%
% Version 2.0: 2013-11-08: Add the part of interpolation of missing IMU
% data, from pospac, we can find the rule
% original imu time :   1.0e+005 *
%   [ 4.300729875569280
%    4.300729925569271
%    4.300730025570000
%    4.300730075569990]
%
% sbet time:  1.0e+005 *
%    [4.300729875569280
%    4.300729925569271
%    4.300729975569271  [interpolated]
%    4.300730025570000
%    4.300730075569990]

if nargin <2
    imu_hz = 200;
    leap_sec = 16; 
end
imu_interval = 1/imu_hz;
IMU_INITIAL_T_LIMIT = 100;
msg_num = 0;
imu_data_new = imu_data_old;
interp_n = 0; % We assume that there are maximum 1000 interpolated epochs, if larger than 1000, error message
modify_n = 0; % We assume that there are maximum 1000 modified epochs, if larger than 1000, error message
interp_array = zeros(1000,1);
modify_array = zeros(1000,1);
% Transform internal time system to GPS time
imu_t_stamp = imu_data_new(:,1);
 qcheck_msg = [];

index_temp = find(imu_t_stamp<IMU_INITIAL_T_LIMIT);
if ~isempty(index_temp)
    t_stamp_gps_start = imu_t_stamp(index_temp(end)+1);
    imu_t_stamp(index_temp) = imu_t_stamp(index_temp)+(t_stamp_gps_start-imu_t_stamp(index_temp(end))-imu_interval);
    str_temp = strcat('The imu time stamp from the first epoch to [',num2str(index_temp(end)),'] has been modified as gps time');
    disp(str_temp);
    msg_num = msg_num+1;
    qcheck_msg{msg_num} = str_temp;
    imu_data_new(:,1) = imu_t_stamp;
    imu_t_stamp = imu_data_new(:,1);
end

% Transform GPS time to UTC time
diff_imu_interval = diff(imu_t_stamp);
index_leap = find(round(abs(diff_imu_interval)) == leap_sec,1);
if ~isempty(index_leap)
    imu_t_stamp(1:index_leap) = imu_t_stamp(1:index_leap)-leap_sec;
    str_temp = strcat('The imu time stamp of GPS time has been trasnformed into UTC time');
    disp(str_temp);
    msg_num = msg_num+1;
    qcheck_msg{msg_num} = str_temp;
    %     imu_data_new = imu_data_new(index_leap+1:end,:);
    %     imu_t_stamp = imu_data_new(:,1);
end

% Checking and modifying IMU data, both time stamp and measurement
imu_data_new(:,1) = imu_t_stamp;
% Case 1: Sunday gps time wrapping
% We solve this problem in gnss data, not here

% Case 2: time stamp error
diff_imu_interval = diff(imu_t_stamp);
index_2 = find(diff_imu_interval>imu_interval*2.8);
if  ~isempty(index_2)
    for i = 1:length(index_2)
        index_1 = index_2(i)-1;
        if diff_imu_interval(index_1)<0
            imu_t_stamp_modified = imu_t_stamp(index_1)+imu_interval;
            imu_t_stamp(index_2(i)) = imu_t_stamp_modified;
            str_temp = strcat('There is an incorrect timestamp at the epoch [',num2str(index_2(i)),' ] gpstime modified as : ',num2str(imu_t_stamp_modified));
            disp(str_temp);
            msg_num = msg_num+1;
            qcheck_msg{msg_num} = str_temp;
            modify_n = modify_n+1;
            modify_array(modify_n) = index_2(i);
        end
    end
    imu_data_new(:,1) = imu_t_stamp;
end

% Case 3: missing data, interpolation of imu data
imu_t_stamp = imu_data_new(:,1);
diff_imu_interval = diff(imu_t_stamp);
index_3 = find(diff_imu_interval>imu_interval*1.5);
m = length(index_3);
n = 0;
for i = 1:m
    index_temp = index_3(i)+1;
    if abs(diff_imu_interval(index_temp)-imu_interval)<imu_interval*0.1
        n = n+1;
    end
end
if n>0
    imu_complementary = zeros(n,7);
    k = 0;
    if  ~isempty(index_3)
        for i = 1:m
            index_temp = index_3(i)+1;
            if abs(diff_imu_interval(index_temp)-imu_interval)<imu_interval*0.1
                k = k+1;
                imu_complementary(k,1) = imu_data_new(index_3(i),1)+imu_interval;
                imu_complementary(k,2:7) = (imu_data_new(index_3(i),2:7)+imu_data_new(index_3(i)+1,2:7))/2;
                str_temp = strcat('An IMU data at the gps time is [',num2str(imu_complementary(k,1) ),' ] interpolated !');
                disp(str_temp);
                msg_num = msg_num+1;
                qcheck_msg{msg_num} = str_temp;
                interp_n = interp_n+1;
                interp_array(interp_n) = index_3(i);
            end
        end
        imu_data_new = [imu_data_new;imu_complementary];
        imu_data_new = sortrows(imu_data_new);
    end
end

% Add imu sampling interval to imu data
interval_array = diff(imu_data_new(:,1));
interval_array = [imu_interval;interval_array];
imu_data_new(:,8) = interval_array;
interp_array(interp_n+1:end,:) = [];
modify_array(modify_n+1:end,:) = [];
Struct_imu_correct.interp_array = interp_array;
Struct_imu_correct.modify_array = modify_array;
