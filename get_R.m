function R_gnss= get_R (pos_std_array)
% Program: create_R 
%
% Purpose: Reconstruct the full covariance matrix of RTKLIB position
% results
%
% Syntax:
%   R_gnss= create_R (pos_std_array)
%
%
% Input arguments:
%   pos_std_array --- The estimated standard deviations of RTKLIB solution.
%                    [std_nn,std_ee,std_uu,std_ne,std_eu,std_un]
%                   (unit: m ,the sign represents the sign of the covariance)
%
% Output arguments:
%   R_gnss       --- The variance-covariance of positioning solution 
%                       [ North, East, Up] 
glv_Re = 6378137;
R_gnss=zeros(3,3);
R_gnss(1,1) = (pos_std_array(1)/glv_Re)^2;
R_gnss(2,2) = (pos_std_array(2)/glv_Re)^2;
R_gnss(3,3) = pos_std_array(3)^2;

% R_gnss=zeros(3,3);
% R_gnss(1,1)=pos_std_array(1)^2;
% R_gnss(2,2)=pos_std_array(2)^2;
% R_gnss(3,3)=pos_std_array(3)^2;
% R_gnss(1,2)=pos_std_array(4)^2*sign(pos_std_array(4));
% R_gnss(2,1)=R_gnss(1,2);
% R_gnss(2,3)=pos_std_array(5)^2*sign(pos_std_array(5));
% R_gnss(3,2)=R_gnss(2,3);
% R_gnss(3,1)=pos_std_array(6)^2*sign(pos_std_array(6));
% R_gnss(1,3)=R_gnss(3,1);
