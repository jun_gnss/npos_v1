function [aim_epoch, aim_epoch_inx]=find_close_epoch(aim_epoch_array,target_epoch)

% Purpose: To find the closest epoch with the targeted epoch


diff_array=aim_epoch_array-target_epoch;
[~, aim_epoch_inx]=min(abs(diff_array));
aim_epoch=aim_epoch_array(aim_epoch_inx);