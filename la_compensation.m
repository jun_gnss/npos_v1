function [posIMU] = la_compensation(qnb,lever,pos1,vn1,posGNSS)
% Program:
%   Transform the GNSS antenna position and velocity to IMU point
% Input
%
% Output
%
%

delta_pos_lever = zeros(3,1); % rad,rad,meter
delta_bOI = lever; 
delta_nOI = q2cnb(qnb)*delta_bOI; %Local frame, ENU
[~,~, ~, retp] = earth_old(pos1, vn1);
delta_pos_lever(1) = delta_nOI(2)/retp.rmh ;
delta_pos_lever(2) = delta_nOI(1)*sec(pos1(1))/retp.rnh;
delta_pos_lever(3) = delta_nOI(3);
%GPS information [extraction] 
posIMU = posGNSS-delta_pos_lever;