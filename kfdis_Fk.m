function [Fikk_1] = kfdis_Fk(Ft, Tkf)

Tkfi = Tkf;
facti = 2;
Fti = Ft;
In = eye(size(Ft,1));
Fikk_1 = In + Tkf*Ft;
Tkfi = Tkfi*Tkf;
Fti = Fti*Ft;
Fikk_1 = Fikk_1 + Tkfi/facti*Fti;
