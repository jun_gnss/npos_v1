function run_rtklib_rtk(FILE_rinex_o_rover,FILE_rinex_o_base,POS_base,FILE_result,CFG_flag) 
% Input
%   FILE_rinex_o_rover
%   FILE_rinex_o_base
%   POS_base
%   FILE_result
%   CFG_flag
%
% Output
%
flight_utc  = RINEX2UTC(FILE_rinex_o_rover);
utc_time_start_pos = (flight_utc.utc_beg) ;
utc_time_end_pos = (flight_utc.utc_end) ;
ds = strcat(num2str(utc_time_start_pos(1)),'/',num2str(utc_time_start_pos(2)),'/',num2str(utc_time_start_pos(3)));
ts = strcat(num2str(utc_time_start_pos(4)),':',num2str(utc_time_start_pos(5)),':',num2str(utc_time_start_pos(6)));
de = strcat(num2str(utc_time_end_pos(1)),'/',num2str(utc_time_end_pos(2)),'/',num2str(utc_time_end_pos(3)));
te = strcat(num2str(utc_time_end_pos(4)),':',num2str(utc_time_end_pos(5)),':',num2str(utc_time_end_pos(6)));
YYYY =num2str(utc_time_start_pos(1));
YY = YYYY(end-1:end);
lat = num2str(POS_base(1),15); %deg
lon = num2str(POS_base(2),15); %deg
hgt = num2str(POS_base(3),15); %h
%**********************************************************
brdc_n_g_file = strrep('D:\Data\GNSS_Products\BRDC\YEAR\brdc*','YEAR',YYYY);
cod_eph_general_file = strrep('D:\Data\GNSS_Products\COD\YEAR\COD*.EPH','YEAR',YYYY);
cod_erp_general_file = strrep('D:\Data\GNSS_Products\COD\YEAR\COD*.ERP','YEAR',YYYY);


fprintf(1,'The RTK process starts...\n');
if CFG_flag == 1 % Fix and hold
    CMD_TEMP_RTKLIB_RTK =strcat('rnx2rtkp -k rtklib_rtk_static_cfg.txt -h -ti 30 -m 7 -l ',{' '},lat,{' '},lon,{' '},hgt,{' '},...
        '-o ',{' '},FILE_result,{' '}, FILE_rinex_o_rover,{' '},FILE_rinex_o_base,{' '},...
        brdc_n_g_file,{' '},cod_eph_general_file,{' '},cod_erp_general_file,{' '},'-ts',{' '},ds,{' '},ts,{' '},'-te',{' '},de,{' '},te);
    system(char(CMD_TEMP_RTKLIB_RTK));
end
if CFG_flag == 2 % Continuous
    CMD_TEMP_RTKLIB_RTK =strcat('rnx2rtkp -k rtklib_rtk_static_cfg.txt -ti 30 -m 7 -l ',{' '},lat,{' '},lon,{' '},hgt,{' '},...
        '-o ',{' '},FILE_result,{' '}, FILE_rinex_o_rover,{' '},FILE_rinex_o_base,{' '},...
        brdc_n_g_file,{' '},cod_eph_general_file,{' '},cod_erp_general_file,{' '},'-ts',{' '},ds,{' '},ts,{' '},'-te',{' '},de,{' '},te);
    system(char(CMD_TEMP_RTKLIB_RTK));
end
fprintf(1,'The RTK result %s has completed! \n',FILE_result);
