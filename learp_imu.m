function [pos_tkgnss,vn_tkgnss] = learp_imu(x1,x2,ts,dt,fb_flag)
% Program: Linear interpolation of imu position and velocity at the given
% epoch
%

% 
% % Forward
% if delta_tgps > 0 
%     pos_tkgps = interp1([0 sampling_step],[pos_old pos_new]',sampling_step+delta_tgps,'linear','extrap')';
%     vn_tkgps = interp1([0 sampling_step],[vn_old vn_new]',sampling_step+delta_tgps,'linear','extrap')';
% else 
%     pos_tkgps = interp1([0 sampling_step],[pos_old pos_new]',sampling_step+delta_tgps,'linear')';
%     vn_tkgps = interp1([0 sampling_step],[vn_old vn_new]',sampling_step+delta_tgps,'linear')'; 
% end
% 
% %  Backforward
% if delta_tgps < 0 
%     pos_tkgps = interp1([ sampling_step 0],[pos_old pos_new]',delta_tgps,'linear','extrap')';
%     vn_tkgps = interp1([ sampling_step 0],[vn_old vn_new]',delta_tgps,'linear','extrap')'; 
% else  
%     pos_tkgps = interp1([ sampling_step 0],[pos_old pos_new]',delta_tgps,'linear')';
%     vn_tkgps = interp1([ sampling_step 0],[vn_old vn_new]',delta_tgps,'linear')'; 
% end