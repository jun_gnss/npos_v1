function file_availability_flag = file_exist_check (Struct_FILE)
file_availability_flag = 0;
if ~exist(Struct_FILE.IMU,'file')
    fprintf(fid_1,'IMU file           : NO\n');
    file_availability_flag = 1;
else
    fprintf(fid_1,'IMU file           : YES\n');
end
if ~exist(Struct_FILE.ROVER_O,'file')
    fprintf(fid_1,'ROVER O file       : NO\n');
    file_availability_flag = 1;
else
    fprintf(fid_1,'ROVER O file       : YES\n');
end
if ~exist(Struct_FILE.ROVER_N,'file')
    fprintf(fid_1,'ROVER N file       : NO\n');
    file_availability_flag = 1;
else
    fprintf(fid_1,'ROVER N file       : YES\n');
end
if ~exist(Struct_FILE.ROVER_G,'file')
    fprintf(fid_1,'ROVER G file       : NO\n');
    file_availability_flag = 1;
else
    fprintf(fid_1,'ROVER G file       : YES\n');
end
if ~exist(Struct_FILE.EPH_1,'file')
    fprintf(fid_1,'EPH_1 file         : NO\n');
    file_availability_flag = 1;
else
    fprintf(fid_1,'EPH_1 file         : YES\n');
end
if ~exist(Struct_FILE.EPH_2,'file')
    fprintf(fid_1,'EPH_2 file         : NO\n');
    file_availability_flag = 1;
else
    fprintf(fid_1,'EPH_2 file         : YES\n');
end
if ~exist(Struct_FILE.EPH_3,'file')
    fprintf(fid_1,'EPH_3 file         : NO\n');
    file_availability_flag = 1;
else
    fprintf(fid_1,'EPH_3 file         : YES\n');
end
if ~exist(Struct_FILE.POSPAC_FSS,'file')
    fprintf(fid_1,'POSPAC_FSS file    : NO\n');
    file_availability_flag = 1;
else
    fprintf(fid_1,'POSPAC_FSS file    : YES\n');
end
for i = 1:length(Struct_BASE)
    if ~exist(Struct_BASE(i).OFILE,'file')
        fprintf(fid_1,'Base %s O file   : NO\n',Struct_BASE(i).NAME);
        file_availability_flag = 1;
    else
        fprintf(fid_1,'Base %s O file   : YES\n',Struct_BASE(i).NAME);
    end
end
if file_availability_flag == 0
    fprintf(fid_1,'Files availability : YES\n');
else
    fprintf(fid_1,'Files availability : NO\n');
end
fprintf(fid_1,'\n');
display('Files availability checking DONE !');