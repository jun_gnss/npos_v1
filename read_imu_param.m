function sImu = read_imu_param(file_imu_specification)
%   Program:
%       Read npos configuration parameters
%   INPUT:
%       IMU_Param.txt
%   OUTPUT:
%       sImu : Struct of IMU hardware specifications

fid = fopen(file_imu_specification);
sImu = struct('IMU_TYPE',[],'IMU_HERTZ',[],...
     'GYRO_BIAS',[],'GYRO_NOISE',[],...
    'GYRO_SCALE_ERROR',[],'GYRO_CORRELATION_TIME',[],  ...
    'ACCELEROMETER_BIAS',[],'ACCELEROMETER_NOISE',[] ,...
    'ACCELEROMETER_SCALE_ERROR',[], 'ACCELEROMETER_CORRELATION_TIME',[]);
while 1
    if (feof(fid)==1)
        break
    end
    lin = fgets(fid);
    if strfind(lin,'%')
        continue;
    end
    lin_cell =  textscan(lin,'%s %s');
    lin_flag = lin_cell{1}(1);
    switch char(lin_flag) 
        case 'IMU_TYPE'
            sImu.IMU_TYPE = char(lin_cell{2});
        case 'IMU_HERTZ'
            sImu.IMU_HERTZ = str2double(lin_cell{2});
        case 'GYRO_BIAS'
            sImu.GYRO_BIAS = str2double(lin_cell{2});
        case 'GYRO_NOISE'
            sImu.GYRO_NOISE = str2double(lin_cell{2});
        case 'GYRO_SCALE_ERROR'
            sImu.GYRO_SCALE_ERROR= str2double(lin_cell{2});
        case 'GYRO_CORRELATION_TIME'
            sImu.GYRO_CORRELATION_TIME= str2double(lin_cell{2});
        case 'ACCELEROMETER_BIAS'
            sImu.ACCELEROMETER_BIAS = str2double(lin_cell{2});
        case 'ACCELEROMETER_NOISE'
            sImu.ACCELEROMETER_NOISE = str2double(lin_cell{2}); 
        case 'ACCELEROMETER_SCALE_ERROR'
            sImu.ACCELEROMETER_SCALE_ERROR = str2double(lin_cell{2});
        case 'ACCELEROMETER_CORRELATION_TIME'
            sImu.ACCELEROMETER_CORRELATION_TIME = str2double(lin_cell{2}); 
    end
end
fclose(fid);