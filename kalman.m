function [Xk, Pk, Kk] = kalman(Phikk_1, Qk, Xk_1, Pk_1, Hk, Rk, Zk)
if nargin<7     % Only processing filter without observation
    Xk = Phikk_1*Xk_1;
    Pk = Phikk_1*Pk_1*Phikk_1'+Qk;
else            % Filter with observation
    Xkk_1 = Phikk_1*Xk_1;
    Pkk_1 = Phikk_1*Pk_1*Phikk_1' + Qk;
    Pxz = Pkk_1*Hk';
    Pzz = Hk*Pxz + Rk;
    
    if rcond(Pzz)<2e-16
        %         display('modifying bad variance matrix');
        ones_matrix = ones (length(Zk)-2,1);
        Pzz = Pzz*diag([1e10;1e10;ones_matrix]); %jun
        Pxz = Pxz*diag([1e10;1e10;ones_matrix]); %jun
        Kk = Pxz/Pzz;                         %jun
        Xk = Xkk_1 + Kk*(Zk-Hk*Xkk_1);        %jun
        Pzz = Pzz/diag([1e10;1e10;ones_matrix]); %jun
        Pk = Pkk_1 - Kk*Pzz*Kk';              %jun
        return
    end
    
    Kk = Pxz/Pzz;
    if isnan(rcond(Pkk_1))
        display(Zk);
        display(diag(Pk_1)');
        error('errors happen -- no numberic values');
    end
    Xk = Xkk_1 + Kk*(Zk-Hk*Xkk_1);
    Pk = Pkk_1 - Kk*Pzz*Kk';
end
