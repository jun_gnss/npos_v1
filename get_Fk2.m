function Fk = get_Fk2(ins)
qnb = ins.qnb;
vn = ins.vn;
pos = ins.pos;
fb = ins.fb;
wb = ins.wib;
method_id = 'EXP1';
reverse_flag = 'forward';
delta_t = ins.ts;

% function Ft = get_Ft(qnb, vn, pos, fb, wb,method_id,reverse_flag)

%   Calculating the system dynamic matrix Ft and spectral density matrix Qt,
%   nosie-input mapping matrix according to different models
%
% method_id:
%     case 'IMU-LN200-15' ,Manual of IMU-LN200
%     case 'Johan_Table7dot1',"Robust navigation with gps/ins and adaptive
%           beamforming", Johan Malmstrom
%     case 'ZhangQuan', "Development and evaluation of gnss/ins data
%           processing software", Zhang Quan
%     case 'Liu_Table4dot1' , "Optimal smoothing techniques in aided
%           inertial navigation and surveying systems", Liu Hang, section
%           2.3.4
%     case 'Chapter7dot2'

if nargin < 7
    reverse_flag = 'forward';
end

if strcmp (reverse_flag,'backward')
    reverse_sign = -1;
else
    reverse_sign = 1;
end
 
glv_f = 1/298.257;
glv_wie = 7.2921151467e-5;
glv_Re = 6378137;
glv_g0 = 9.7803267714;
glv_e = sqrt(2*glv_f-glv_f^2);
glv_e2 = glv_e^2;

wie = glv_wie;
wie = reverse_sign * wie; 

%-----------------------------------------

sl=sin(pos(1)); cl=cos(pos(1)); tl=sl/cl; sl2=sl*sl; sl4=sl2*sl2;
wnie = [0; glv_wie*cl; glv_wie*sl];
sq = 1-glv_e2*sl2; sq2 = sqrt(sq);
RMh = glv_Re*(1-glv_e2)/sq/sq2+pos(3);
RNh = glv_Re/sq2+pos(3);
wnen = [-vn(2)/RMh; vn(1)/RNh; vn(1)/RNh*tl]; 

wnie = wnie * reverse_sign;
secl = 1/cl; secl2 = secl^2;
f_RMh = 1/RMh; f_RNh = 1/RNh; f_RMh2 = f_RMh^2; f_RNh2 = f_RNh^2;
%-----------------------------------------
 
% ***** Form F_INS ***********
M1 = [0, -f_RMh, 0; f_RNh, 0, 0; f_RNh*tl, 0, 0];
Mp = [0, 0, 0; -wie*sl, 0, 0; wie*cl, 0, 0];
Mpp = [0, 0, vn(2)*f_RMh2; 0, 0, -vn(1)*f_RNh2; vn(1)*secl2*f_RNh, 0, -vn(1)*tl*f_RNh2];
M2 = Mp+Mpp;
M3 = askew(vn)*M1 - askew(2*wnie+wnen);
M4 = askew(vn)*(2*Mp+Mpp);
M5 = [0, f_RMh, 0; secl*f_RNh, 0, 0; 0, 0, 1];
M6 = [0, 0, -vn(2)*f_RMh2; vn(1)*secl*tl*f_RNh, 0, -vn(1)*secl*f_RNh2; 0, 0, 0];
S1 = -askew(wnie+wnen);
S2 = askew(qmulv(qnb,fb));
o3 = zeros(3,3);   Cnb = q2cnb(qnb);
I3 = eye(3,3);
Fins = [ S1    M1    M2
    S2    M3    M4
    o3    M5    M6  ];

switch method_id
    
     case 'EXP1' %
        correlation_time_bg_reciprocal  = 1/3600;
        correlation_time_ba_reciprocal  = 1/3600;
        correlation_time_sg_reciprocal  = 0;
        correlation_time_sa_reciprocal  = 0;
        
        Ft = zeros (21,21);
        F2 = [-Cnb      o3      -Cnb*diag(wb)   o3;
            o3        Cnb     o3          Cnb*diag(fb);
            o3        o3      o3              o3];
        F3 = -diag([correlation_time_bg_reciprocal;correlation_time_bg_reciprocal;correlation_time_bg_reciprocal;...
            correlation_time_ba_reciprocal;correlation_time_ba_reciprocal;correlation_time_ba_reciprocal;...
            correlation_time_sg_reciprocal;correlation_time_sg_reciprocal;correlation_time_sg_reciprocal;...
            correlation_time_sa_reciprocal;correlation_time_sa_reciprocal;correlation_time_sa_reciprocal]);
                 
        Ft(1:9,1:9) = Fins;
        Ft(1:9,10:21) = F2;
        Ft(10:21,10:21) = F3;
   
        Fk = kfdis_Fk(Ft, delta_t);
    otherwise
        
end

