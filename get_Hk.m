function     Hk = get_Hk(method_id,pos_id,ins)

% if strcmp(method_id,'EXP2') && strcmp(pos_id,'POS') % Estimation LeverArm
%     Hk = [zeros(3,6), eye(3), zeros(3,6), -ins.MpvCnb, zeros(3,18)];
% end
if nargin <3
    ins = [];
    if strcmp(method_id,'EXP1') && strcmp(pos_id,'POS')
        Hk = [zeros(3,6), eye(3), zeros(3,12)];
    end
end