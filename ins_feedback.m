function [ins_new,Xk] = ins_feedback(ins_old,Xk,method_id)

if strcmp(method_id,'EXP1')
    
    ins_new.pos(3) = ins_old.pos(3)-Xk(9,1);
    ins_new.pos(2) = ins_old.pos(2)-Xk(8,1);
    ins_new.pos(1) = ins_old.pos(1)-Xk(7,1);
    ins_new.vn(3) = ins_old.vn(3)-Xk(6,1);
    ins_new.vn(2) = ins_old.vn(2)-Xk(5,1);
    ins_new.vn(1) = ins_old.vn(1)-Xk(4,1);
    ins_new.qnb = qdelphi(ins_old.qnb, Xk(1:3,1));
    ins_new.eb = ins_old.eb - Xk(10:12);
    ins_new.db = ins_old.db - Xk(13:15);
    ins_new.Kg = ins_old.Kg-Xk(16:18);
    ins_new.Ka = ins_old.Ka-Xk(19:21); 
    Xk(1:21,1) = 0;
end
