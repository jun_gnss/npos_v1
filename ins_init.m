function ins = ins_init(avp0, ts ,epoch)

[qnb0, vn0, pos0] = setvals(a2qua(avp0(1:3)), avp0(4:6), avp0(7:9));
ins.ts = ts; ins.time = epoch;
[ins.qnb, ins.vn, ins.pos] = setvals(qnb0, vn0, pos0);
[ins.qnb, ins.att, ins.Cnb] = attsyn(ins.qnb);
ins.avp  = [ins.att; ins.vn; ins.pos];

ins.Cnb0 = ins.Cnb;
ins.avp  = [ins.att; ins.vn; ins.pos];
ins.eth = ethinit(ins.pos, ins.vn);
[ins.eb, ins.db,ins.Kg, ins.Ka] = setvals(zeros(3,1));
ins.tauG = inf(3,1); ins.tauA = inf(3,1); % gyro & acc correlation time
