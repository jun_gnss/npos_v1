function disp_progress(i,n,sum,fb_flag)
if mod(i,n) == 0
    if strcmp(fb_flag,'forward')
        fprintf(1,'RSGPS forward filter has processed epochs   : %f   .\n',i);
        fprintf(1,'RSGPS forward filter progress has completed : %f%% .\n',100*i/sum);
    elseif strcmp(fb_flag,'backward')
        fprintf(1,'RSGPS backward filter has processed epochs   : %f   .\n',i);
        fprintf(1,'RSGPS backward filter progress has completed : %f%% .\n',(100-100*i/sum));
    end
end