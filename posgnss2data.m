function [data_gnss,pos_std_array,vel_std_array]=posgnss2data (posgnss_file)
% Program: posgnss2data
%
% Notice:  The previous version is posgnss2cins
%
% Purpose: Reading the coordinates information from POSGNSS .cmb file
%
% Syntax:
%   [data_gnss,pos_std,vel_std]=posgnss2data(posgnss_file);
%
% Input arguments:
%   posgnss_file --- The .cmb result from POSGNSS (part of POSPAC)
%
% Output arguments:
%   data_gnss  --- [gps time,lat,lon,height,vel_E,vel_N,vel_U]
%                   (unit: sow,deg,deg,m,m/s,m/s,m/s)
%   pos_std    --- [std_nn,std_ee,std_uu,std_ne,std_eu,std_un]
%                   (unit: m ,the sign represents the sign of the covariance)
%   vel_std    --- [std_ee,std_nn,std_uu,std_en,std_nu,std_ue]
%                   (unit: m/s ,the sign represents the sign of the covariance)
%
% ----------------------------------------------------------------------
% File.....: posgnss2data.m
% Date.....: 29-MAY-2013
% Version..: 1.0
% Author...: Jun Wang
%            ROAMES
% ----------------------------------------------------------------------


fid=fopen(posgnss_file);
sow_flag='Out {	Tim:'; % GPS time, seconds of week
pos_flag='Geo:'; %Latitude, Longitude, Height, degree and meter
vel_flg='Loc:'; % Local velocity North, East, Height
var_flag='Var:'; % Variance of position and velcocity, North, East, Height

epoch_pos=[];
plh_pos=[];% degree, degree, meter
vel_pos=[];% East,north,height
var_pos=[];%
var_vel=[];
epoch_num=0;

while 1
    lin=fgets(fid);
    if  strcmp(lin(1),';')
        continue;
    end
    if (feof(fid)==1)
        break
    end
    if strfind(lin,sow_flag) % Epoch Information
        epoch_num=epoch_num+1;
        lin_cell=textscan(lin,'%s%s%s%f%f%d');
        epoch_pos(epoch_num,1)=lin_cell{4};
        continue;
    end
    if strfind(lin,pos_flag) % Position Information, radian,radian,meter
        lin_cell=textscan(lin,'%s%f%f%f%f%f%f%f%f');
        pos_temp=([lin_cell{2},lin_cell{3},lin_cell{4}]);%Latitude
        plh_pos(epoch_num,1)=dms2deg(pos_temp);
        pos_temp=([lin_cell{5},lin_cell{6},lin_cell{7}]);%Longitude
        plh_pos(epoch_num,2)=dms2deg(pos_temp);
        pos_temp=lin_cell{8};%Height
        plh_pos(epoch_num,3)=pos_temp;
        continue;
    end
    if strfind(lin,vel_flg) % Velocity Information
        lin_cell=textscan(lin,'%s%f%f%f%f%f%f');
        vel_pos(epoch_num,1)=lin_cell{5};%East
        vel_pos(epoch_num,2)=lin_cell{6};%North
        vel_pos(epoch_num,3)=lin_cell{7};%Height
        continue;
    end
    if strfind(lin,var_flag) % Variance Information
        lin_cell=textscan(lin,'%s%f%f%f%f%f%f%f');
        var_pos(epoch_num,1)=lin_cell{2};%East
        var_pos(epoch_num,2)=lin_cell{3};%North
        var_pos(epoch_num,3)=lin_cell{4};%Height
        var_vel(epoch_num,1)=lin_cell{5};%V_East
        var_vel(epoch_num,2)=lin_cell{6};%V_North
        var_vel(epoch_num,3)=lin_cell{7};%V_Height
        continue;
    end
end
fclose(fid); 
data_gnss(1:epoch_num,1)=epoch_pos(1:epoch_num,1);
data_gnss(1:epoch_num,2:4)=plh_pos(1:epoch_num,1:3);
data_gnss(1:epoch_num,5:7)=vel_pos(1:epoch_num,[1,2,3]);

pos_std_array=zeros(epoch_num,6);
vel_std_array=zeros(epoch_num,6);

pos_std_array(1:epoch_num,1:3)=sqrt(var_pos(1:epoch_num,[2,1,3]));
vel_std_array(1:epoch_num,1:3)=sqrt(var_vel(1:epoch_num,[1,2,3])); 