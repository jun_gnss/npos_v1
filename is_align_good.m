function  [align_flag,sAline] = is_align_good(sInsOld,sInsNew)

att_diff_threshold = 0.015*pi/180; 
pos_norm_threshold = 0.15;
att_norm_threshold = 0.20;
glv_Re = 6378137;
h_flight = 500;
%  ins.avp  = [ins.att; ins.vn; ins.pos];
att_old = sInsOld.att;
pos_old = sInsOld.pos;
vn_old = sInsOld.vn;
att_new = sInsNew.att;
pos_new = sInsNew.pos;
vn_new = sInsNew.vn;

pos_diff = abs(pos_old - pos_new);
pos_diff(1:2) = pos_diff(1:2)*glv_Re;
pos_diff_norm = norm(pos_diff);

vn_diff = abs(vn_old - vn_new);  

att_diff = aa2phi(att_old, att_new);
att_diff_norm = max(abs(att_diff))*h_flight;

sAline.att_diff = att_diff;
sAline.pos_diff = pos_diff;
sAline.vn_diff = vn_diff;

try
if pos_diff_norm < pos_norm_threshold && max(abs(att_diff)) < att_diff_threshold && att_diff_norm < att_norm_threshold
    align_flag = 1;
    fprintf(1,'The difference between two alignment result is smaller than threshold!\n');
    fprintf(1,'Fine alignment successfully done!\n');
else
   align_flag = 0; 
end
catch
   disp; 
end