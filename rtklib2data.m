function [t_pos_vel_array,pos_std_array,vel_std_array,S_data]= rtklib2data (rtklib_file)
% Program: rtklib2data
%
% Notice: (1) The previous vesion is called rtklib2cins
%         (2) Reference: RTKLIB manual_2.4.2 Appendix B File Formats
%
% Purpose: Reading the coordinates information from RTKLIB
%
% Syntax:
%   [data_gnss,pos_std,vel_std]=rtklib2data(rtklib_file);
%
%
% Input arguments:
%   rtklib_file --- The .pos result from RTKLIB, attention: the input format
%   must be as the below one
%
%    GPST            latitude(d'")   longitude(d'")  height(m)   Q  ns   sdn(m)   sde(m)   sdu(m)  sdne(m)  sdeu(m)  sdue(m) age(s)  ratio
% 1738 248420.000  -22 46 37.75663  147 37 20.44444   326.0978   2   9   0.3855   0.4290   0.9989   0.0575  -0.3374   0.1384   0.00   14.1
%
% Output arguments:
%   t_pos_vel_array  --- [gps time,lat,lon,height,vel_E,vel_N,vel_U]
%                   (unit: sow,deg,deg,m,m/s,m/s,m/s)
%   pos_std    --- [std_nn,std_ee,std_uu,std_ne,std_eu,std_un]
%                   (unit: m ,the sign represents the sign of the covariance)
%   vel_std    --- [std_ee,std_nn,std_uu,std_en,std_nu,std_ue]
%                   (unit: m/s ,the sign represents the sign of the covariance)
%
% ----------------------------------------------------------------------
% File.....: rtklib2data.m
% Date.....: 29-MAY-2013
% Version..: 1.0
% Author...: Jun Wang 
% ----------------------------------------------------------------------

fid=fopen(rtklib_file);
glvs;
%Determine the row number of a file
Nrows = numel(textread(rtklib_file,'%1c%*[^\n]'));
gps_week = zeros(Nrows,1);
gps_sec  = zeros(Nrows,1);
lat_deg  = zeros(Nrows,1);
lat_min  = zeros(Nrows,1);
lat_sec = zeros(Nrows,1);
lon_deg  = zeros(Nrows,1);
lon_min  = zeros(Nrows,1);
lon_sec  = zeros(Nrows,1);
h  = zeros(Nrows,1);
Q  = zeros(Nrows,1);
ns  = zeros(Nrows,1);
sdn  = zeros(Nrows,1);
sde  = zeros(Nrows,1);
sdu  = zeros(Nrows,1);
sdne  = zeros(Nrows,1);
sdeu  = zeros(Nrows,1);
sdun  = zeros(Nrows,1);
age  = zeros(Nrows,1);
ratio = zeros(Nrows,1);


plh_pos = zeros(Nrows,3);
std_pos = zeros(Nrows,6);




epoch_num=0;


while 1
    lin=fgets(fid);
    if  strcmp(lin(1),'%')
        continue;
    end
    epoch_num=epoch_num+1;
    lin_cell=textscan(lin,'%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f');
    gps_week(epoch_num,1)=lin_cell{1};
    gps_sec(epoch_num,1)=lin_cell{2};
    lat_deg(epoch_num,1)=lin_cell{3};
    lat_min(epoch_num,1)=lin_cell{4};
    lat_sec(epoch_num,1)=lin_cell{5};
    lon_deg(epoch_num,1)=lin_cell{6};
    lon_min(epoch_num,1)=lin_cell{7};
    lon_sec(epoch_num,1)=lin_cell{8};
    h(epoch_num,1)=lin_cell{9};
    Q(epoch_num,1)=lin_cell{10};
    ns(epoch_num,1)=lin_cell{11};
    sdn(epoch_num,1)=lin_cell{12};
    sde(epoch_num,1)=lin_cell{13};
    sdu(epoch_num,1)=lin_cell{14};
    sdne(epoch_num,1)=lin_cell{15};
    sdeu(epoch_num,1)=lin_cell{16};
    sdun(epoch_num,1)=lin_cell{17};
    age(epoch_num,1)=lin_cell{18};
    ratio(epoch_num,1)=lin_cell{19};
    pos_temp=([lin_cell{3},lin_cell{4},lin_cell{5}]);%Latitude
    plh_pos(epoch_num,1)=dms2deg(pos_temp);
    pos_temp=([lin_cell{6},lin_cell{7},lin_cell{8}]);%Longitude
    plh_pos(epoch_num,2)=dms2deg(pos_temp);
    pos_temp=lin_cell{9};%Height
    plh_pos(epoch_num,3)=pos_temp;
    
    std_temp=([lin_cell{12},lin_cell{13},lin_cell{14}]);%std n/e/u
    std_pos(epoch_num,1:3)=std_temp;
    std_temp=([lin_cell{15},lin_cell{16},lin_cell{17}]);%std n/e/u
    std_pos(epoch_num,4:6)=std_temp;
    ratio(epoch_num,1)=lin_cell{19};
    if (feof(fid)==1)
        break
    end
end
fclose(fid);
% Data Struct
n = epoch_num;
S_data = struct('gps_week',zeros(n,1),'gps_sec',zeros(n,1),...
    'lat_deg',zeros(n,1),'lat_min',zeros(n,1),'lat_sec',zeros(n,1),...
    'lon_deg',zeros(n,1),'lon_min',zeros(n,1),'lon_sec',zeros(n,1),...
    'h',zeros(n,1),...
    'Q',zeros(n,1),'ns',zeros(n,1),...
    'sdn',zeros(n,1),'sde',zeros(n,1),'sdu',zeros(n,1),...
    'sdne',zeros(n,1),'sdeu',zeros(n,1),'sdun',zeros(n,1),...
    'age',zeros(n,1),'ratio',zeros(n,1));
S_data.gps_week = gps_week(1:n,1);
S_data.gps_sec = gps_sec(1:n,1);
S_data.lat_deg = lat_deg(1:n,1);
S_data.lat_min = lat_min(1:n,1);
S_data.lat_sec = lat_sec(1:n,1);
S_data.lon_deg = lon_deg(1:n,1);
S_data.lon_min = lon_min(1:n,1);
S_data.lon_sec = lon_sec(1:n,1);
S_data.h = h(1:n,1);
S_data.Q = Q(1:n,1);
S_data.ns = ns(1:n,1);
S_data.sdn = sdn(1:n,1);
S_data.sde = sde(1:n,1);
S_data.sdu = sdu(1:n,1);
S_data.sdne = sdne(1:n,1);
S_data.sdeu = sdeu(1:n,1);
S_data.sdun = sdun(1:n,1);
S_data.age = age(1:n,1);
S_data.ratio = ratio(1:n,1);



%*********************************************************

t_pos_vel_array(1:epoch_num,1)=gps_sec(1:epoch_num,1);
t_pos_vel_array(1:epoch_num,2:4)=plh_pos(1:epoch_num,1:3);
% Transform the RTKLIB position information to velocity
% See. EH Shin "An unscented kalman filter for in-motion alignment of
% low-cost imus" (44-46)
vel_ENU=zeros(epoch_num,3);
for i = 2:epoch_num
    pos_temp = t_pos_vel_array(i,2:4);
    pos_temp(1:2) = pos_temp(1:2)*glv.deg;
    [~, ~, ~, retp] = earth(pos_temp);
    lat_mid = 0.5*(t_pos_vel_array(i,2)+t_pos_vel_array(i-1,2))*glv.deg;
    h_mid   = 0.5*(t_pos_vel_array(i,4)+t_pos_vel_array(i-1,4));
    lat_diff = (t_pos_vel_array(i,2)-t_pos_vel_array(i-1,2))*glv.deg;
    lon_diff = (t_pos_vel_array(i,3)-t_pos_vel_array(i-1,3))*glv.deg;
    h_diff   = t_pos_vel_array(i,4)-t_pos_vel_array(i-1,4);
    vel_ENU(i,1) = lon_diff*(retp.rnh+h_mid)*cos(lat_mid);
    vel_ENU(i,2) = lat_diff*(retp.rmh+h_mid);
    vel_ENU(i,3) = h_diff;
end

% vel_ENU(2:end,3)=diff(data_gnss(:,4));%Up
% vel_ENU(2:end,2)=diff(data_gnss(:,2)).*111323;%North
% vel_ENU(2:end,1)=diff(data_gnss(:,3)).*111135;% East

vel_ENU(1,1:3)=vel_ENU(2,1:3);
t_pos_vel_array(:,5:7)=vel_ENU;

pos_std_array(1:epoch_num,1:6)=std_pos(1:epoch_num,1:6); %North,East,Up
vel_std_array=zeros(size(pos_std_array));
for i=2:epoch_num
    vel_std_array(i,1)=sqrt(pos_std_array(i,2)^2+pos_std_array(i-1,2)^2);%East
    vel_std_array(i,2)=sqrt(pos_std_array(i,1)^2+pos_std_array(i-1,1)^1);%North
    vel_std_array(i,3)=sqrt(pos_std_array(i,3)^2+pos_std_array(i-1,3)^2); %Up
end
vel_std_array(1,:)=vel_std_array(2,:);




