% Program: read_sbet(sbet_file)
% Purpose: 
% SBET FORMAT:
%         double gpsTime seconds;
%         double latitude radians;
%         double longitude radians;
%         double ellipsoidalAltitude meters;
%         double xVelocity meters/second;
%         double yVelocity meters/second;
%         double zVelocity meters/second;
%         double roll radians;
%         double pitch radians;
%         double heading radians;
%         double wanderAngle radians;
%         double xAcceleration meters/second2;;
%         double yAcceleration meters/second2;
%         double zAcceleration meters/second2;
%         double xAngularRate radians/second;
%         double yAngularRate radians/second;
%         double zAngularRate radians/second;


function data=read_sbet(sbet_file,sbet_txt_file)
if nargin==1
    sbet_txt_file=[];
end
inFile=fopen(sbet_file);
data=fread(inFile,inf,'double');
fclose(inFile);clear inFile;
% Rearrange data to one record per row
data=reshape(data,17,[])';
if nargin==2
save (sbet_txt_file, 'data', '-ascii', '-double') ;
end