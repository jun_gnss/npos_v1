function [dataSBET, dataRMS ] = npos2sbet(dataXk,dataPk,sLC)

n = size(dataXk,1);
dataSBET = zeros(n,17);
dataRMS = zeros(n,10);

ma = sLC.sLaMa.MOUNTING_ANGLE *pi/180;
Crefimu = a2cnb(ma);
Cimuref = Crefimu';
for t = 1:n
    pitch_roll_yaw_imu = dataXk(t,2:4);
    Cnimu = a2cnb(pitch_roll_yaw_imu);
    Cnref = Cnimu*Cimuref;
    pitch_roll_yaw_ref = m2att(Cnref);
    dataXk(t,2:4) = pitch_roll_yaw_ref;
    dataXk(t,4) = -dataXk(t,4);%Transform the yaw sign as common definition
end
 
dataSBET(:,1) = dataXk(:,1);
dataSBET(:,2) = dataXk(:,8);
dataSBET(:,3) = dataXk(:,9);
dataSBET(:,4) = dataXk(:,10);
dataSBET(:,5) = dataXk(:,5);
dataSBET(:,6) = dataXk(:,6);
dataSBET(:,7) = dataXk(:,7);
dataSBET(:,8) = dataXk(:,3);
dataSBET(:,9) = dataXk(:,2);
dataSBET(:,10) = dataXk(:,4);

dataRMS(:,1) = dataXk(:,1);
dataRMS(:,2:10) = dataPk(:,1:9);
 