function Fk = get_Fk(ins,method_id)

Ft_15 = etm(ins); %     fi     dvn    dpos    eb       db

if strcmp(method_id,'EXP1')
    %    fi     dvn    dpos    eb       db    sg   sa
    Ft = zeros (21,21);
end

% tauG_reciprocal  = 1./ins.tauG;
% tauA_reciprocal  = 1./ins.tauA;
 
F2 = [   -ins.Cnb*diag(ins.wib)   zeros(3,3);
    zeros(3,3)          ins.Cnb*diag(ins.fb);];

% F3 = -diag([tauG_reciprocal;tauA_reciprocal;...
%     0;0;0;...
%     0;0;0]);

Ft(1:15,1:15) = Ft_15;
Ft(1:6,16:21) = F2;
% Ft(16:21,16:21) = F3;

Fk = kfdis_Fk(Ft, ins.ts);

% Fk = Ft*ins.ts;
% if ins.nts>0.1  % for large time interval, this may be more accurate.
%     Fk = expm(Fk);
% else   % Fk = I + Ft*nts + 1/2*(Ft*nts)^2  , 2nd order expension
%     Fk = eye(size(Ft)) + Fk;% + Fk*Fk*0.5;
% end