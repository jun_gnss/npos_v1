function comb_rtklib(pos_rtklib_file1,pos_rtklib_file2,new_pos_file,comb_mode)
% The direction of pos_rtklib_file1 is 'forward' as default
% comb_mode : site-site or forward-backward
%
%
% Comments: we assume pos_rtklib_file1 is forward direction!!!


if nargin < 4
    comb_mode = 'site-site';
end
if isempty(pos_rtklib_file1)
    copyfile(pos_rtklib_file2,new_pos_file);
    return;
end
%   data_gnss  --- [gps time,lat,lon,height,vel_E,vel_N,vel_U]
%                   (unit: sow,deg,deg,m,m/s,m/s,m/s)
%   pos_std    --- [std_nn,std_ee,std_uu,std_ne,std_eu,std_un]
%                   (unit: m ,the sign represents the sign of the covariance)

[data_rtklib_1_temp,pos_std_1_temp,~,S_data_1] = rtklib2data (pos_rtklib_file1);
[data_rtklib_2_temp,pos_std_2_temp,~,S_data_2] = rtklib2data (pos_rtklib_file2);
gps_sec_1 = S_data_1.gps_sec;
gps_sec_2 = S_data_2.gps_sec;

gps_week_array   = S_data_1.gps_week;
gps_second_array = S_data_1.gps_sec;
q_flag_array     = S_data_1.Q;
q_flag_array_1     = S_data_1.Q;
q_flag_array_2     = S_data_2.Q;
num_sat_array    = S_data_1.ns;
age_array        = S_data_1.age;
ratio_array      = S_data_1.ratio;

if length(gps_sec_1) < 300 || length(gps_sec_2) < 300
    disp('Warning: The GNSS position epochs is less than 5 minutes !');
    return;
else
    [~,index_temp] = sort(gps_sec_1);
    if index_temp(1)>index_temp(end)
        direction_1 = 'forward';
    else
        direction_1 = 'backward';
    end
    [~,index_temp] = sort(gps_sec_2);
    if index_temp(1)>index_temp(end)
        direction_2 = 'forward';
    else
        direction_2 = 'backward';
    end
end
%  Guarantee the combinte file is following the FORWARD direction
index_wrap_1 = find (gps_sec_1 == 0,1);
if isempty(index_wrap_1)
    flag_wrap_1 = 0;
else
    flag_wrap_1 = 1;
    if strcmp(direction_1,'forward')
        gps_sec_1(index_wrap_1:end) = gps_sec_1(index_wrap_1:end)+604800;
    else
        gps_sec_1(1:index_wrap_1) = gps_sec_1(1:index_wrap_1)+604800;
    end
end
index_wrap_2 = find (gps_sec_2 == 0,1);
if isempty(index_wrap_2)
    flag_wrap_2 = 0;
else
    flag_wrap_2 = 1;
    if strcmp(direction_2,'forward')
        gps_sec_2(index_wrap_2:end) = gps_sec_2(index_wrap_2:end)+604800;
    else
        gps_sec_2(1:index_wrap_2) = gps_sec_2(1:index_wrap_2)+604800;
    end
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

[~,index_1,index_2] = intersect(gps_sec_1,gps_sec_2);

data_rtklib_1 = data_rtklib_1_temp(index_1,:);
pos_std_1 = pos_std_1_temp(index_1,:);
q_flag_array_1     = q_flag_array_1(index_1,:);
data_rtklib_2 = data_rtklib_2_temp(index_2,:);
pos_std_2 = pos_std_2_temp(index_2,:);
q_flag_array_2     = q_flag_array_2(index_2,:);

gps_week_array   = gps_week_array(index_1,:);
gps_second_array = gps_sec_1(index_1,:);
% q_flag_array     = q_flag_array(index_1,:);
num_sat_array    = num_sat_array(index_1,:);
age_array        = age_array(index_1,:);
ratio_array      = ratio_array(index_1,:);

data_rtklib_comb = data_rtklib_1;
pos_std_comb = pos_std_1;

for i = 1:length(data_rtklib_1)
    % If the Q flag of the two file is not different, we directly use the
    % smaller value of Q flag position information, otherwise, if the Q
    % flags are the same, we do the smoothing
    if q_flag_array_2(i,1) ~= 6 && q_flag_array_1(i,1) < q_flag_array_2(i,1)        
        data_rtklib_comb(i,2:4) = data_rtklib_1(i,2:4);
        pos_std_comb(i,1:6) = pos_std_1(i,1:6);
        q_flag_array(i,1:6) = q_flag_array_1(i,1);
        continue;
    elseif q_flag_array_1(i,1) ~= 6 && q_flag_array_1(i,1) > q_flag_array_2(i,1)        
        data_rtklib_comb(i,2:4) = data_rtklib_2(i,2:4);
        pos_std_comb(i,1:6) = pos_std_2(i,1:6);
        q_flag_array(i,1:6) = q_flag_array_2(i,1);
        continue;
    end
    % Latitude
    w1_lat = 1/(pos_std_1(i,1)+0.0001)^2;
    w2_lat = 1/(pos_std_2(i,1)+0.0001)^2;
    w_lat_sum = w1_lat+w2_lat;
    a1 = w1_lat/w_lat_sum;
    a2 = w2_lat/w_lat_sum;
    %     lat_temp
    
    % Longitude
    w1_lon = 1/(pos_std_1(i,2)+0.0001)^2;
    w2_lon = 1/(pos_std_2(i,2)+0.0001)^2;
    w_lon_sum = w1_lon+w2_lon;
    b1 = w1_lon/w_lon_sum;
    b2 = w2_lon/w_lon_sum;
    
    % Height
    w1_h = 1/(pos_std_1(i,3)+0.0001)^2;
    w2_h = 1/(pos_std_2(i,3)+0.0001)^2;
    w_h_sum = w1_h+w2_h;
    c1 = w1_h/w_h_sum;
    c2 = w2_h/w_h_sum;
    
    A_mat = [a1 a2 0 0 0 0; 0 0 b1 b2 0 0; 0 0 0 0 c1 c2];
    X_mat = [data_rtklib_1(i,2),data_rtklib_2(i,2),data_rtklib_1(i,3),data_rtklib_2(i,3),data_rtklib_1(i,4),data_rtklib_2(i,4)]';
    P_mat = blkdiag(pos_std_1(i,1)^2,pos_std_2(i,1)^2,pos_std_1(i,2)^2,pos_std_2(i,2)^2,pos_std_1(i,3)^2,pos_std_2(i,3)^2);
    P_mat(1,[3,5]) = pos_std_1(i,[4,6]);
    P_mat(2,[4,6]) = pos_std_2(i,[4,6]);
    P_mat(3,[1,5]) = pos_std_1(i,[4,5]);
    P_mat(4,[2,6]) = pos_std_2(i,[4,5]);
    P_mat(5,[1,3]) = pos_std_1(i,[6,5]);
    P_mat(6,[2,4]) = pos_std_2(i,[6,5]);
    Y_mat   = A_mat*X_mat;
    P_Y_mat = A_mat*P_mat*A_mat';
    
    data_rtklib_comb(i,2:4) = Y_mat';
    temp_value = sqrt(diag(P_Y_mat))';
    pos_std_comb(i,1:6) = [temp_value,P_Y_mat(1,2),P_Y_mat(2,3),P_Y_mat(3,1)];
end
  
% Generate new RTKLIB position file
flag_1 = '% solution  :';
flag_2 = '% ref pos   :';
flag_3 = '%  GPST';
fidin = fopen(pos_rtklib_file1);
fidout = fopen(new_pos_file,'w');

while ~feof(fidin)
    lin = fgets(fidin);
    if strfind(lin,flag_1)
        fprintf(fidout,'%s %s \n',flag_1,'combined');
        continue;
    end
    if strfind(lin,flag_2)
        if  strcmp(comb_mode,'site-site')
            fprintf(fidout,'%s %s \n',flag_2,'multiple base stations');
            continue;
        else
            fprintf(fidout,'%s',lin);
            continue;
        end
    end
    if strfind(lin,flag_3)
        fprintf(fidout,'%s',lin);
        for i = 1:length(data_rtklib_1)
            gps_week   = gps_week_array(i);
            gps_second = gps_second_array(i);
            if gps_second>=604800
                gps_second = gps_second-604800;
            end
            lat_dms = degrees2dms(data_rtklib_comb(i,2));
            lat_deg = lat_dms(1);
            lat_min = lat_dms(2);
            lat_sec = lat_dms(3);
            lon_dms = degrees2dms(data_rtklib_comb(i,3));
            lon_deg = lon_dms(1);
            lon_min = lon_dms(2);
            lon_sec = lon_dms(3);
            height =  (data_rtklib_comb(i,4));
            q_flag     = q_flag_array(i);
            num_sat    = num_sat_array(i);
            std_array = pos_std_comb(i,1:6);
            age        = age_array(i);
            ratio      = ratio_array(i);
            fprintf(fidout,'%4d%11.3f%5d%3d%9.5f%5d%3d%9.5f%11.4f%4d%4d%9.4f%9.4f%9.4f%9.4f%9.4f%9.4f%7.2f%7.1f\n',...
                gps_week,gps_second,lat_deg,lat_min,lat_sec,lon_deg,lon_min,lon_sec,height,q_flag,num_sat,std_array,age,ratio);
            
        end
        break;
    end
    fprintf(fidout,'%s',lin);
end

fclose(fidin);
fclose(fidout);