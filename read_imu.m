function dataIMU = read_imu(imuFileName)

% Read imu data from LN-200 IMU and convert into physical units. dataIMU
% has columns of time, gyro output and accelerometer output in units of
% delta angle [radians] and delta v [m/s]

fid = fopen(imuFileName, 'r');
% Read into 32 x N array of byte records
buf = reshape(fread(fid, Inf, 'uint8=>uint8'), 32, []);
fclose(fid);
% Reinterpret each 32 byte chunk of raw data as [double, 6*int32]
time = typecast(reshape(buf(1:8,:), [], 1), 'double');
intData = reshape(typecast(reshape(buf(9:32,:), [], 1), 'int32'), 6, [])';

% IMU data is encoded in binary fixed point format, with 18 and 14
% fractional binary digits for gyro and accelerometer respectively.
deltaOmega    = double(intData(:,1:3)) .* (2^-18);
deltaVelocity = double(intData(:,4:6)) .* (2^-14);

dataIMU = [time, deltaOmega, deltaVelocity];
