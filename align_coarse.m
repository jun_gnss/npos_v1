function [sIns_coarse,sQmsg] = align_coarse(dataIMU,dataGNSS,sLC,fb_flag)
% Program:
%   align_coarse 
%
Vn_east_threshold = sLC.COARSE_VE_LIMIT;
Vn_north_threshold = sLC.COARSE_VN_LIMIT;
lever = sLC.sLaMa.LEVER_ARM' ;
tauG = sLC.sImu.GYRO_CORRELATION_TIME; 
tauA = sLC.sImu.ACCELEROMETER_CORRELATION_TIME; 
% Determine the reference gnss epoch utc
index_vel_east  = find(abs(dataGNSS.pos_vel(:,4))>Vn_east_threshold);
index_vel_north = find(abs(dataGNSS.pos_vel(:,5))>Vn_north_threshold);
switch upper(fb_flag)
    case 'FORWARD'
        epoch_start = min(index_vel_east(1),index_vel_north(1)); 
    case 'BACKWARD'
        epoch_start = max(index_vel_east(end),index_vel_north(end)); 
end
time_utc_start = dataGNSS.epoch(epoch_start,1);
if time_utc_start<dataIMU(1,1)
    time_utc_start = dataIMU(1,1);
    [~,epoch_start] = find_close_epoch(dataGNSS.epoch(:,1),time_utc_start);
else
    if time_utc_start>dataIMU(end,1);
        time_utc_start = dataIMU(end,1);
        [~,epoch_start] = find_close_epoch(dataGNSS.epoch(:,1),time_utc_start);
    end
end
 
%*************************************************************************
%  COARSE ALIGNMENT: in Motion
%*************************************************************************
[time_coarse] = find_close_epoch(dataIMU(:,1),time_utc_start);
ts = dataIMU(2,1) - dataIMU(1,1);
coarse_yaw_imu = -atan2(dataGNSS.pos_vel(epoch_start,4),dataGNSS.pos_vel(epoch_start,5));
coarse_pitch_imu = atan2(dataGNSS.pos_vel(epoch_start,6),norm([dataGNSS.pos_vel(epoch_start,4),dataGNSS.pos_vel(epoch_start,5)]));
coarse_roll_imu = 5*pi/180;
coarse_att_imu = [coarse_pitch_imu;coarse_roll_imu;coarse_yaw_imu]; %Radian 
coarse_pos_gnss = [dataGNSS.pos_vel(epoch_start,1);dataGNSS.pos_vel(epoch_start,2);dataGNSS.pos_vel(epoch_start,3)];
coarse_vel_gnss = [dataGNSS.pos_vel(epoch_start,4);dataGNSS.pos_vel(epoch_start,5);dataGNSS.pos_vel(epoch_start,6)];

avp0 = [coarse_att_imu;coarse_vel_gnss;coarse_pos_gnss];

sIns_coarse = ins_init(avp0, ts,time_coarse);
% [coarse_pos_imu] = la_compensation(sIns_coarse,lever,coarse_pos_gnss,coarse_vel_gnss);
[coarse_pos_imu] = la_compensation(sIns_coarse.qnb,lever,  coarse_pos_gnss,coarse_vel_gnss,coarse_pos_gnss);
coarse_vel_imu = coarse_vel_gnss;
avp0 = [coarse_att_imu;coarse_vel_imu;coarse_pos_imu];
% sIns_coarse = insinit(avp0, ts);
sIns_coarse = ins_init(avp0, ts,time_coarse); 
% Correlation time
sIns_coarse.tauG = repmat(tauG,3,1); sIns_coarse.tauA = repmat(tauA,3,1); % gyro & acc correlation time
sQmsg = [];