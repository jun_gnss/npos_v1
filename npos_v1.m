% function [dataSBET,dataRMS] = npos_v1(SESSION)
SESSION = '13120803';
 
%   (1) Initialization
addpath('I:\Program\NPOS\psins150315\base');
glvs;
fprintf(1,'NPOS Initialization starts...\n');
[sFile,sNpos,sImu,sLaMa,sRtklib,sBase,sIgs] = npos_initialization(SESSION); 
fprintf(1,'NPOS Initialization done!\n');

%   (2) Preparing data
fprintf(1,'IMU data loading and checking starts...\n');
%   (2-1) IMU data
dataIMU = read_imu(sFile.FILE_IMU);
% IMU body frame transform:
dataIMU(:,2:7) = imurfu(dataIMU(:,2:7), 'FRD');
%   (2-2) IMU data quality
imu_hz = sImu.IMU_HERTZ;
leap_sec = sNpos.LEAP_SECONDS;
[dataIMU_OLD,~] = qcheck_imu(dataIMU,imu_hz,leap_sec);
% delete some data
k1 = sNpos.SECONDS_DELETE_IMU*sImu.IMU_HERTZ;
k2 = k1+sNpos.MISSION_HOURS*3600*sImu.IMU_HERTZ;
if k2<length(dataIMU)
    dataIMU = dataIMU_OLD(k1:k2,:);
else
    dataIMU = dataIMU_OLD(k1:end,:);
end
% write_log(sFile,sQmsg2,step_id);
fprintf(1,'IMU data loading and checking done!\n');

%   (3) Processing GNSS
fprintf(1,'GNSS Calculation starts...\n');
[dataGNSS,sQmsg3] = npos_gnss(sFile,sBase,sRtklib,sIgs); 
dataGNSS = gpstime_check(dataGNSS); 
fprintf(1,'GNSS Calculation done!\n');

%   (4) Processing PSINS
fprintf(1,'GNSS/IMU Loose Coulped Processing starts...\n');
sLC = npos_initLC(sNpos,sImu,sLaMa);
%   (4-1) Forward IMU/GNSS
fprintf(1,'Forward Filtering Processing...\n');
[dataXk_f,dataRMS_f,~] = npos_filter(dataIMU,dataGNSS,sLC,'forward');

%   (4-2) Reverse IMU/GNSS
fprintf(1,'Backward Filtering Processing...\n');
[dataXk_b,dataRMS_b,~] = npos_filter(dataIMU,dataGNSS,sLC,'backward');

%   (4-3) NPOS_avp fusion  
[dataNpos,nposRMS] = npos_fusion(dataXk_f,dataRMS_f,dataXk_b,dataRMS_b);

%  (5) Transform to SBET
[dataSBET, sbetRMS ] = npos2sbet(dataNpos,nposRMS,sLC);
 
fprintf(1,'SBET file has been saved!\n');
write_sbet(dataSBET,sbetRMS,sFile,SESSION);
 