function [dataXk,dataPk,ins] = npos_lc(dataIMU,dataGNSS,ins,sLC )

% Program: npos_lc - NPOS Loose Coupled, using the IMU data and GNSS
%   position result to get SBET data and RMS
%
% Input:
%   dataIMU - IMU data n*7 [t,wx,wy,wz,ax,ay,az]
%   dataGNSS - structure GNSS result
%   sAlign - struct of IMU initial information as well as lever arm
%   sLC - struct of Loose Coulped,
%
% Output:

% (1) Time synchronization
% af_flag = sLC.type; %Loose couple for aligning or filtering
fb_flag = sLC.fb_flag;
method_id = sLC.METHOD_ID;
ts0 = 1/sLC.IMU_HERTZ;
kf_obs = sLC.OBS_ID;
lever = sLC.sLaMa.LEVER_ARM' ;
t_start = ins.time;
glvs;
% Initialization based on af_flag and fb_flag

[~,idx_start] = find_close_epoch(dataIMU(:,1),t_start);
reverse = reverse_flag(fb_flag);
kf = kf_initial(ins,sLC);
%
if strcmp(fb_flag,'forward')
    idx_end = size(dataIMU,1)-1;
elseif strcmp(fb_flag,'backward')
    idx_end = 2;
    ins.vn = -ins.vn; ins.eb = -ins.eb;
end
len = abs(idx_start-idx_end)+1;

if strcmp(method_id,'EXP1')
    Xk = zeros(21,1);
    dataXk = zeros(len,22);
    dataPk = zeros(len,22);
    obs_err = zeros(len,3);
    [eb, db,Kg, Ka] = setvals(zeros(3,1));     
end

% (2) Filtering
kf_num = 0;
qnb = ins.qnb; vn = ins.vn; pos = ins.pos;
qnb_old = qnb;  vn_old = vn;   pos_old = pos;
qnb_new = qnb;  vn_new = vn;   pos_new = pos;
Pxk = get_Pxk(method_id,af_flag);

if strcmp(fb_flag,'forward')
    % Direction: forward
    for i = idx_start:reverse:idx_end
        disp_progress(i,60000,len); 
        [wm,vm,ts] = imu_compensation (dataIMU(i:i+reverse,1:8),ins,reverse); 
        % Get GNSS EKF Tag
        if i ~=idx_end
            [tag_ekf,dt,gnss_idx] = ekf_tag(dataIMU(i:i+reverse,1),ts0,dataGNSS.epoch,fb_flag);
        end
        if tag_ekf == 1
            kf_num = kf_num+1;
            pos_tkgps = interp1([0 ts],[pos_old pos_new]',dt,'linear','extrap')';
            vn_tkgps = interp1([0 ts],[vn_old vn_new]',dt,'linear','extrap')';
            posGNSS = dataGNSS.pos_vel(gnss_idx,1:3)';
            [posIMU] = la_compensation(qnb,lever,pos_tkgps,vn_tkgps,posGNSS); 
            Ft = get_Ft(qnb_new, vn_new, pos_new, vm(:,2)/ts, wm(:,2)/ts,method_id,'forward');
            [Fikk_1, Qk] = get_Fk_Qk(Ft,ts,method_id); 
            Zk = pos_tkgps-posIMU;
            obs_err(kf_num,1:3) = Zk;
            Rk = get_R(dataGNSS.pos_std(gnss_idx,:));
            Pxk = diag_posdef(Pxk);
            Hk = get_Hk(method_id,kf_obs);
%             [Xk, Pxk] = kalman(Fikk_1, Qk, Xk, Pxk, Hk, Rk, Zk);
           [Xk, Pxk] = kfupdate2(Fikk_1, Qk, Xk, Pxk, Hk, Rk, Zk);
           ins_old = ins;
           [ins,Xk] = ins_feedback(ins,Xk,method_id);
           
%             pos(3) = pos_new(3)-Xk(9,1);
%             pos(2) = pos_new(2)-Xk(8,1);
%             pos(1) = pos_new(1)-Xk(7,1);
%             vn(3) = vn_new(3)-Xk(6,1);
%             vn(2) = vn_new(2)-Xk(5,1);
%             vn(1) = vn_new(1)-Xk(4,1);
%             qnb = qdelphi(qnb_new, Xk(1:3,1));
%             eb = eb - Xk(10:12);
%             db = db - Xk(13:15);
%             Kg = Kg-Xk(16:18);
%             Ka = Ka-Xk(19:21);
%             ins.Kg = Kg;
%             ins.eb = eb;
%             ins.Ka = Ka;
%             ins.db = db;            
%             Xk(1:21,1) = 0;

%             qnb_old = qnb;
%             vn_old = vn;
%             pos_old = pos;
            
            [qnb,vn,pos] = sins_forward(ins_old,wm,vm,ts); 
            ins.qnb = qnb; ins.vn = vn; ins.pos = pos;
%             [qnb,vn,pos] = sins_forward(ins.qnb_old,ins.vn_old,ins.pos_old,wm,vm,ts); 
            qnb_new = qnb;
            vn_new = vn;
            pos_new = pos;
              
            Ft = get_Ft(qnb_new, vn_new, pos_new, vm(:,2)/ts, wm(:,2)/ts,method_id,'forward');
            [Fikk_1, Qk] = get_Fk_Qk(Ft,ts,method_id);
            Pxk = diag_posdef(Pxk);
            [Xk, Pxk] = kalman(Fikk_1, Qk, Xk, Pxk);
        else
%             qnb_old = qnb;
            vn_old = vn;
            pos_old = pos;
            [qnb,vn,pos] = sins_forward(qnb,vn,pos,wm,vm,ts); 
            qnb_new = qnb;
            vn_new = vn;
            pos_new = pos;
             
            Ft = get_Ft(qnb_new, vn_new, pos_new, vm(:,2)/ts, wm(:,2)/ts,method_id,'forward');
            [Fikk_1, Qk] = get_Fk_Qk(Ft,ts,method_id);
            
            Pxk = diag_posdef(Pxk);
            [Xk, Pxk] = kalman(Fikk_1, Qk, Xk, Pxk);
        end
        data_RSGPS_filter(i+1,1) = dataIMU(i+1,1);
        data_RSGPS_filter(i+1,2:4) = q2att(qnb); %  Attitude: Pitch, Roll, Yaw [rad/rad/rad]
        data_RSGPS_filter(i+1,5:7) = vn; % Velocity: East,North, Up [meter/second]
        data_RSGPS_filter(i+1,8:10) = pos; % Position: latitude,longitude,height [rad/rad/meter] 
        % For smoothing
        var_Xk_filter(i+1,1:9) = [Pxk(1,1),Pxk(2,2),Pxk(3,3),Pxk(4,4),Pxk(5,5),Pxk(6,6),Pxk(7,7),Pxk(8,8),Pxk(9,9)];
    end
    ins.qnb = qnb;
    ins.vn = vn;
    ins.pos = pos;
    ins.time = dataIMU(i+1,1);
    var_Xk_filter(1,:) = var_Xk_filter(2,:);
    
elseif strcmp(fb_flag,'backward')
    for i = idx_start:reverse:idx_end
        
    end
end

disp('Loose coupled function done');
