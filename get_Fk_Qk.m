function [Fk, Qk] = get_Fk_Qk(Ft,delta_t,method_id)

%   Calculating the system transition matrix Fk and system covariance matrix Qk
%
%   The function is smiliar to kfdis.m which is discretization function for
%   continuous KF equation
%
% method_id:
%     case 'IMU-LN200-15' ,Manual of IMU-LN200
%     case 'Johan_Table7dot1',"Robust navigation with gps/ins and adaptive
%           beamforming", Johan Malmstrom
%     case 'ZhangQuan', "Development and evaluation of gnss/ins data
%           processing software", Zhang Quan
%     case 'Liu_Table4dot1' , "Optimal smoothing techniques in aided
%           inertial navigation and surveying systems", Liu Hang, section
%           2.3.4
%     case 'Chapter7dot2'


glv_deg = pi/180;
glv_dpsh = glv_deg/60;              %
glv_ugpsHz =  9.7803267714*1e-6;
glv_ug =  9.7803267714*1e-6;
glv_deg = pi/180;

o3 = zeros(3,3);
switch method_id
    case 'EXP1'
        GYRO_NOISE = 0.008*glv_dpsh;
        ACCE_NOISE = 100*glv_ugpsHz;
        bg = 0.1*glv_deg/3600;
        ba = 150*glv_ug;
        correlation_time_bg_reciprocal  = 1/3600;
        correlation_time_ba_reciprocal  = 1/3600;
        
        qarw = ((GYRO_NOISE*sqrt(delta_t))^2+(bg*delta_t)^2)*eye(3,3);
        qvrw =((ACCE_NOISE*sqrt(delta_t))^2+(ba*delta_t)^2)*eye(3,3);   
        qbg = GYRO_NOISE^2*(1-exp(-2*delta_t*correlation_time_bg_reciprocal))*eye(3,3);
        qba = ACCE_NOISE^2*(1-exp(-2*delta_t*correlation_time_ba_reciprocal))*eye(3,3);  
        Qk = zeros(21,21);
        Qk(1:3,1:3) = qarw;
        Qk(4:6,4:6) = qvrw;  
        Qk(10:12,10:12) = qbg;
        Qk(13:15,13:15) = qba; 
           
        Fk = kfdis_Fk(Ft, delta_t);
                  
    otherwise
        
end