
 
function cmp_result=cmp_2sbet_data(data_sbet1,data_sbet2)
glvs
PITCH_LIMIT_1 = 0.006 ;
ROLL_LIMIT_1 = 0.006 ;
YAW_LIMIT_1 = 0.01;
LAT_LIMIT_1 = 0.1;
LON_LIMIT_1 = 0.1;
HEIGHT_LIMIT_1 = 0.1;
PITCH_LIMIT_2 = 0.01;
ROLL_LIMIT_2 = 0.01;
YAW_LIMIT_2 = 0.02;
LAT_LIMIT_2 = 0.2;
LON_LIMIT_2 = 0.2;
HEIGHT_LIMIT_2 = 0.2;
data_sbet1(:,1) = round(data_sbet1(:,1)*1e5)/1e5;
data_sbet2(:,1) = round(data_sbet2(:,1)*1e5)/1e5;
[~,ia,ib] = intersect(data_sbet1(:,1),data_sbet2(:,1));
  
cmp_result.cmp_time = data_sbet1(ia,1);
diff_temp = data_sbet1(ia,:)-data_sbet2(ib,:);

cmp_result.diff_lat = diff_temp(:,2);
cmp_result.diff_lon = diff_temp(:,3);
cmp_result.diff_h   = diff_temp(:,4);

cmp_result.diff_vx = diff_temp(:,5);
cmp_result.diff_vy = diff_temp(:,6);
cmp_result.diff_vz = diff_temp(:,7);
 
cmp_result.diff_roll  = diff_temp(:,8);
cmp_result.diff_pitch = diff_temp(:,9);
data_sbet_yaw = diff_temp(:,10)-diff_temp(:,11); 
for t = 1:length(cmp_result.cmp_time)
    if data_sbet_yaw(t,1) < 0;
        data_sbet_yaw(t,1) = data_sbet_yaw(t,1)+2*pi;
    end
end
I1 = find(data_sbet_yaw<-pi);
data_sbet_yaw(I1,1) = data_sbet_yaw(I1,1)+2*pi;
I2 = find(data_sbet_yaw>pi);
data_sbet_yaw(I2,1) = data_sbet_yaw(I2,1)-2*pi;
cmp_result.diff_yaw = data_sbet_yaw;
  
cmp_result.stat.mean_att = mean([cmp_result.diff_pitch cmp_result.diff_roll cmp_result.diff_yaw])*180/pi;% Unit: degree
cmp_result.stat.mean_pos = mean([cmp_result.diff_lat cmp_result.diff_lon cmp_result.diff_h]);% Unit: rad/rad/meter
cmp_result.stat.mean_pos_meter = [cmp_result.stat.mean_pos(1)*glv.Re cmp_result.stat.mean_pos(2)*glv.Re cmp_result.stat.mean_pos(3)];
cmp_result.stat.mean_vel = mean([cmp_result.diff_vx cmp_result.diff_vy cmp_result.diff_vz]);
cmp_result.stat.std_att = std([cmp_result.diff_pitch cmp_result.diff_roll cmp_result.diff_yaw])*180/pi;
cmp_result.stat.std_pos = std([cmp_result.diff_lat cmp_result.diff_lon cmp_result.diff_h]);
cmp_result.stat.std_pos_meter = [cmp_result.stat.std_pos(1)*glv.Re cmp_result.stat.std_pos(2)*glv.Re cmp_result.stat.std_pos(3)];
cmp_result.stat.std_vel = std([cmp_result.diff_vx cmp_result.diff_vy cmp_result.diff_vz]);
cmp_result.stat.max_att = max(abs([cmp_result.diff_pitch cmp_result.diff_roll cmp_result.diff_yaw]))*180/pi;% Unit: degree
cmp_result.stat.max_pos = max(abs([cmp_result.diff_lat cmp_result.diff_lon cmp_result.diff_h]));% Unit: rad/rad/meter
cmp_result.stat.max_pos_meter = [cmp_result.stat.max_pos(1)*glv.Re cmp_result.stat.max_pos(2)*glv.Re cmp_result.stat.max_pos(3)];
cmp_result.stat.max_vel = max(abs([cmp_result.diff_vx cmp_result.diff_vy cmp_result.diff_vz]));
bad_pitch = sum(abs(cmp_result.diff_pitch)*180/pi>PITCH_LIMIT_1)/length(cmp_result.diff_pitch);
bad_roll = sum(abs(cmp_result.diff_roll)*180/pi>ROLL_LIMIT_1)/length(cmp_result.diff_roll);
bad_yaw = sum(abs(cmp_result.diff_yaw)*180/pi>YAW_LIMIT_1)/length(cmp_result.diff_yaw);
cmp_result.ratio.bad_att = [bad_pitch,bad_roll,bad_yaw];
bad_lat = sum(abs(cmp_result.diff_lat)*glv.Re>LAT_LIMIT_1)/length(cmp_result.diff_lat);
bad_lon = sum(abs(cmp_result.diff_lon)*glv.Re>LON_LIMIT_1)/length(cmp_result.diff_lon);
bad_h = sum(abs(cmp_result.diff_h)>HEIGHT_LIMIT_1)/length(cmp_result.diff_h);
cmp_result.ratio.bad_pos = [bad_lat,bad_lon,bad_h];
clear bad_pitch bad_roll bad_yaw bad_lat bad_lon bad_h
bad_pitch = sum(abs(cmp_result.diff_pitch)*180/pi>PITCH_LIMIT_2)/length(cmp_result.diff_pitch);
bad_roll = sum(abs(cmp_result.diff_roll)*180/pi>ROLL_LIMIT_2)/length(cmp_result.diff_roll);
bad_yaw = sum(abs(cmp_result.diff_yaw)*180/pi>YAW_LIMIT_2)/length(cmp_result.diff_yaw);
cmp_result.ratio2.bad_att = [bad_pitch,bad_roll,bad_yaw];
bad_lat = sum(abs(cmp_result.diff_lat)*glv.Re>LAT_LIMIT_2)/length(cmp_result.diff_lat);
bad_lon = sum(abs(cmp_result.diff_lon)*glv.Re>LON_LIMIT_2)/length(cmp_result.diff_lon);
bad_h = sum(abs(cmp_result.diff_h)>HEIGHT_LIMIT_2)/length(cmp_result.diff_h);
cmp_result.ratio2.bad_pos = [bad_lat,bad_lon,bad_h];
