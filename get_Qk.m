function Qk = get_Qk(sImu,ts,method_id)
  
glv_deg = pi/180;
glv_dpsh = glv_deg/60;              %
glv_ugpsHz =  9.7803267714*1e-6;
glv_ug =  9.7803267714*1e-6;
glv_deg = pi/180;

web = sImu.GYRO_NOISE*glv_dpsh;
wdb = sImu.ACCELEROMETER_NOISE*glv_ugpsHz;
bg = sImu.GYRO_BIAS*glv_deg/3600;
ba = sImu.ACCELEROMETER_BIAS*glv_ug;
correlation_time_bg_reciprocal  = 1/sImu.GYRO_CORRELATION_TIME;
correlation_time_ba_reciprocal  = 1/sImu.ACCELEROMETER_CORRELATION_TIME;
switch method_id
    case  'EXP1'
        qarw = ((web*sqrt(ts))^2+(bg*ts)^2)*eye(3,3);
        qvrw =((wdb*sqrt(ts))^2+(ba*ts)^2)*eye(3,3);
        qbg = web^2*(1-exp(-2*ts*correlation_time_bg_reciprocal))*eye(3,3);
        qba = wdb^2*(1-exp(-2*ts*correlation_time_ba_reciprocal))*eye(3,3);
        Qk = zeros(21,21);
        Qk(1:3,1:3) = qarw;
        Qk(4:6,4:6) = qvrw;
        Qk(10:12,10:12) = qbg;
        Qk(13:15,13:15) = qba;
end
