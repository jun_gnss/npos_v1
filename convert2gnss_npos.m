function dataGNSS = convert2gnss_npos(pos_vel,pos_std,vel_std,sNpos)
%
%   Input: 
%       pos_vel: t,lat,lon,height,ve,vn,vu
%
%   Output:
%
leap_sec = sNpos.LEAP_SECONDS; 
gnss_skip_sec = sNpos.SECONDS_DELETE_GNSS; % According to experience, the first and last 60 seconds quality is bad

 dataGNSS.epoch = pos_vel(gnss_skip_sec:end-gnss_skip_sec,1) ;

index_temp = find (dataGNSS.epoch(:,1)==0,1);
if ~isempty(index_temp) % We assume the data_gnss is forward here
    dataGNSS.epoch(index_temp:end,1) = dataGNSS.epoch(index_temp:end,1)+604800;
    disp('The GNSS data is across two gps weeks, so those epochs of new gps week has been added 604800 seconds');
end
fprintf('GNSS data has transform to UTC time system\n');
dataGNSS.epoch = dataGNSS.epoch- leap_sec;

pos_vel(:,2:3) = pos_vel(:,2:3)*pi/180;
dataGNSS.pos_vel = pos_vel(gnss_skip_sec:end-gnss_skip_sec,2:7);
dataGNSS.pos_std = pos_std(gnss_skip_sec:end-gnss_skip_sec,1:6);
dataGNSS.vel_std = vel_std(gnss_skip_sec:end-gnss_skip_sec,1:6);
