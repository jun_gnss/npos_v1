function    [wm,vm,ts] = imu_compensation (data_imu,ins,reverse)
%
% IMU measurement calibration with bias compensation and scale factor;
%
%

% Measurement compensation
wm = zeros(2,3);
vm = zeros(2,3);
sg_compensation = ins.Kg;
bg_compensation = ins.eb;
sa_compensation = ins.Ka;
ba_compensation = ins.db;
n_sample = size(data_imu,1);

for i = 1:n_sample
    wm(i,:) = reverse*(eye(3)+diag(sg_compensation))*data_imu(i,2:4)'+ bg_compensation*data_imu(i,8);
    vm(i,:) = (eye(3)+diag(sa_compensation))*data_imu(i,5:7)' + ba_compensation*data_imu(i,8);
end
ts = data_imu(1,8);

wm = wm';
vm = vm';